package com.engefour.strateponto.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class ViewPagerLeaderboardAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    //Quando estiver na posição x, estancia um Fragment com aquele campo de pesquisa
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ViewPagerLeaderboardFragment.newInstance("semanaHoras")
            1 -> ViewPagerLeaderboardFragment.newInstance("mesHoras")
            2 -> ViewPagerLeaderboardFragment.newInstance("anoHoras")
            3 -> ViewPagerLeaderboardFragment.newInstance("semanaPassadaHoras")
            else -> {
                return ViewPagerLeaderboardFragment.newInstance("mesPassadoHoras")
            }
        }
    }

    override fun getCount(): Int {
        return 5
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Essa semana"
            1 -> "Esse mês"
            2 -> "Esse ano"
            3 -> "Semana passada"
            else -> {
                return "Mês Passado"
            }
        }
    }
}