package com.engefour.strateponto.viewpager

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.engefour.strateponto.activities.MainActivity
import com.engefour.strateponto.R
import com.google.firebase.firestore.FirebaseFirestore
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.crashlytics.android.Crashlytics
import com.engefour.strateponto.uteis.LoadingDialog
import com.google.firebase.firestore.Query
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.listview_membros.view.*
import kotlinx.android.synthetic.main.row_membros.view.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import java.util.*

private const val ARG_PARAM1 = "campoPesquisa"

class ViewPagerMembrosFragment: Fragment(){
    private lateinit var campoPesquisa:String
    private var listener: OnFragmentInteractionListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
                campoPesquisa = it.getString(ARG_PARAM1)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.listview_membros, container, false)
        val db = FirebaseFirestore.getInstance()
        val adapter = GroupAdapter<ViewHolder>()
        view.listViewMembros.adapter = adapter

        //Pega as informação de todos os membros. Campo "Todos" do Membros
        if(campoPesquisa == "Todos") {
            db.collection("usuarios").orderBy("userNome", Query.Direction.ASCENDING)
                .whereEqualTo("isActive",true).addSnapshotListener { documentSnapshot, error ->
                    if (error != null) {
                        Crashlytics.logException(error)
                    } else if (documentSnapshot != null && context!=null) {
                        adapter.clear()
                        documentSnapshot.forEach { usuario ->
                            val imageUrl = usuario.get("userFotoUrl").toString()
                            val userNome = usuario.get("userNome").toString()
                            val isOnline = usuario.get("isOnline") as Boolean
                            val userId = usuario.get("userId").toString()
                            if (activity != null)
                                adapter.add(
                                    UserItem(
                                        imageUrl,
                                        userNome,
                                        userId,
                                        isOnline,
                                        activity
                                    )
                                )
                        }
                        val main = activity as MainActivity?
                        main?.atualizarListaMembros()
                    }
                }
            // Campo "IsOnline" e "IOS" do Membros (de acordo com o campoPesquisa)
            // É separado pq precisa do whereEqualTo = true
        }else {
            db.collection("usuarios").whereEqualTo(campoPesquisa, true)
                .whereEqualTo("isActive",true).orderBy("userNome", Query.Direction.ASCENDING)
                .addSnapshotListener { documentSnapshot, error ->
                    if (error != null) {
                        Crashlytics.logException(error)
                    } else if (documentSnapshot != null && context!=null) {
                        adapter.clear()
                        documentSnapshot.forEach { usuario ->
                            val imageUrl = usuario.get("userFotoUrl").toString()
                            val userNome = usuario.get("userNome").toString()
                            val isOnline = usuario.get("isOnline") as Boolean
                            val userId = usuario.get("userId").toString()
                            if(activity != null)
                            adapter.add(
                                UserItem(
                                    imageUrl,
                                    userNome,
                                    userId,
                                    isOnline,
                                    activity
                                )
                            )
                        }
                            val main = activity as MainActivity?
                            main?.atualizarListaMembros()
                    }
                }
        }
        //Depois que acaba a query, atualiza a lista
        view.listViewMembros.layoutManager = LinearLayoutManager(activity)
        return view
    }

    //Classe de UserItem do Groupie
    //Diferente do Leaderboard, que precisa ter um adapter só pra isso por precisar organizar por segundos
    //Popula a view e faz o login/logout
    class UserItem(private val imageUrl:String, private val userNome:String, private val userId:String,private val isOnline:Boolean, private val activity: Activity?): Item<ViewHolder>(){
        private lateinit var loadingDialog: LoadingDialog
        override fun bind(viewHolder: ViewHolder, position: Int) {
            //Se tiver online, bolinha verde, se não, bolinha vermelha
            if (!isOnline)
                viewHolder.itemView.onlineCircle.background =
                    ContextCompat.getDrawable(activity!!, R.drawable.circle_shape_offline)
            else
                viewHolder.itemView.onlineCircle.background =
                    ContextCompat.getDrawable(activity!!, R.drawable.circle_shape_online)


            viewHolder.itemView.textViewMembroNome.text = userNome
            Glide.with(viewHolder.itemView).load(imageUrl).into(viewHolder.itemView.imageViewMembroFoto)

            //Faz login ou logout dependendo se o membro tá online ou não
            //Manda um AlertDialog pra caso o usuário dê misclick
            viewHolder.itemView.setOnClickListener {
                if (isOnline) {
                    val builder = AlertDialog.Builder(activity)
                    builder.setTitle("Obrigado por ajudar!")
                    builder.setMessage("Deseja Finalizar o plantão de \n$userNome ?")
                    builder.setPositiveButton("Sim") { _, _ ->
                        loadingDialog = LoadingDialog(activity)
                        loadingDialog.showDialog()
                        logOut(userId)
                    }

                    builder.setNegativeButton("Cancelar") { _, _ ->}
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                } else {
                    val builder = AlertDialog.Builder(activity)
                    builder.setTitle("Iniciar Plantão")
                    builder.setMessage("Deseja Iniciar o plantão de \n$userNome ?")
                    builder.setPositiveButton("Sim") { _, _ ->
                        loadingDialog = LoadingDialog(activity)
                        loadingDialog.showDialog()
                        logIn(userId)
                    }
                    builder.setNegativeButton("Cancelar") { _, _ -> }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }
        }

        override fun getLayout(): Int {
            return R.layout.row_membros
        }

        // Faz o logout de um usuário na lista de membros
        // Código tá ruim. Talvez reaproveitar o login/logout do PontoFragment de algum jeito? Copiei o código de lá
        // Pega o horário do id que tá aberto e fecha. Não tem como fugir, mas pode usar menos código
        private fun logOut(uid:String){

            val db = FirebaseFirestore.getInstance()
            val userRef =  db.document("usuarios/$uid")
            userRef.get().addOnSuccessListener {
                // Pega o horário de entrada e compara com o de saída pra fazer o cálculo de segundos
                val horarioEntrada = it.get("horaEntrada").toString()
                val dateEntrada = DateTime.parse(horarioEntrada,
                    DateTimeFormat.forPattern("dd-MM-yy hh.mm.ss aa").withLocale(Locale("pt","BR")))
                val p = Period(dateEntrada, DateTime.now(DateTimeZone.getDefault()))
                var segundosTotais = p.toStandardSeconds().seconds
                if (segundosTotais < 60) {
                    loadingDialog.hideDialog()
                    userRef.update("isOnline", false)
                } else {
                    // 12 horas de trabalho é o máximo por um plantão. Impede que alguém esqueça de bater ponto na saída
                    if (segundosTotais > 43200)
                        segundosTotais = 43200
                    val usuarioHorario = HashMap<String, Any>()

                    //Se o ultimo plantão foi no ano passado
                    if(DateTime.now(DateTimeZone.getDefault()).year != it.get("anoUltimoPlantao").toString().toInt()){
                        //Esse ano tem o horário total desse plantão
                        usuarioHorario["anoHoras"] = segundosTotais
                        usuarioHorario["anoUltimoPlantao"] = DateTime.now(DateTimeZone.getDefault()).year

                        // Se o ultimo plantão nesse ano, só soma ao total
                    }else
                        usuarioHorario["anoHoras"] = it.get("anoHoras").toString().toInt() + segundosTotais

                    //Se o último plantão foi mês passado
                    if(DateTime.now(DateTimeZone.getDefault()).monthOfYear != it.get("mesUltimoPlantao").toString().toInt()){

                        //Vê se faz mais de um mês desde o ultimo plantão
                        if(it.get("mesUltimoPlantao").toString().toInt() < (DateTime.now(DateTimeZone.getDefault()).monthOfYear - 1) )
                            usuarioHorario["mesPassadoHoras"] = 0
                        else
                            usuarioHorario["mesPassadoHoras"] = it.get("mesHoras").toString().toInt()

                        //Esse mês tem o horário total desse plantão
                        usuarioHorario["mesHoras"] = segundosTotais
                        usuarioHorario["mesUltimoPlantao"] = DateTime.now(DateTimeZone.getDefault()).monthOfYear

                        // Se o ultimo plantão nesse mês, só soma ao total
                    }else
                        usuarioHorario["mesHoras"] = it.get("mesHoras").toString().toInt() + segundosTotais


                    //Se o ultimo plantão foi semana passada
                    if(DateTime.now(DateTimeZone.getDefault()).weekOfWeekyear != it.get("semanaUltimoPlantao").toString().toInt()){
                        //Vê se faz mais de uma semana desde o ultimo plantão
                        if(it.get("semanaUltimoPlantao").toString().toInt() < (DateTime.now(DateTimeZone.getDefault()).weekOfWeekyear - 1) )
                            usuarioHorario["semanaPassadaHoras"] = 0
                        else
                            usuarioHorario["semanaPassadaHoras"] = it.get("semanaHoras").toString().toInt()

                        //Essa semana tem o horário total desse plantão
                        usuarioHorario["semanaHoras"] = segundosTotais
                        usuarioHorario["semanaUltimoPlantao"] = DateTime.now(DateTimeZone.getDefault()).weekOfWeekyear

                        //Se o ultimo plantão foi nessa semana, só soma ao total
                    } else
                        usuarioHorario["semanaHoras"] = it.get("semanaHoras").toString().toInt() + segundosTotais

                    usuarioHorario["isOnline"] = false

                    userRef.update(usuarioHorario)
                        .addOnSuccessListener {
                            loadingDialog.hideDialog()
                            Toast.makeText(activity,"Plantão contabilizado!", Toast.LENGTH_SHORT).show()
                        }
                        .addOnFailureListener{ error ->
                            loadingDialog.hideDialog()
                            Toast.makeText(activity,"Falha ao registrar plantão: " + error.message, Toast.LENGTH_SHORT)
                                .show()
                        }
                }
            }
        }

        //Faz entrada no horário do plantão
        private fun logIn(uid: String){
            val usuarioHorario = HashMap<String, Any>()
            val horaCompleta = DateTime.now(DateTimeZone.getDefault()).toString("dd-MM-yy hh.mm.ss aa",Locale("pt", "BR"))
            usuarioHorario["horaEntrada"] = horaCompleta
            usuarioHorario["isOnline"] = true
            val db = FirebaseFirestore.getInstance()
            db.document("usuarios/$uid").update(usuarioHorario)
                .addOnSuccessListener {
                    Toast.makeText(activity,"Plantão iniciado!", Toast.LENGTH_SHORT).show()
                    loadingDialog.hideDialog()
                }
                .addOnFailureListener{
                    loadingDialog.hideDialog()
                    Toast.makeText(activity,"Falha ao registrar plantão: " + it.message, Toast.LENGTH_SHORT)
                        .show()
                }
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(campoPesquisa: String?) =
            ViewPagerMembrosFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, campoPesquisa)
                }
            }
    }
}
