package com.engefour.strateponto.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class ViewPagerGerenciarAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    //Quando estiver na posição x, estancia um Fragment com aquele campo de pesquisa
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ViewPagerAdminFragment.newInstance("Membros Ativos")
            else -> {
                return ViewPagerAdminFragment.newInstance("Membros Inativos")
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Membros Ativos"
            else -> {
                return "Membros Inativos"
            }
        }
    }
}