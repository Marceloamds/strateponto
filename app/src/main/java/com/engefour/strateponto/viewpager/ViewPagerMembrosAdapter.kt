package com.engefour.strateponto.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class ViewPagerMembrosAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    //Quando estiver na posição x, estancia um Fragment com aquele campo de pesquisa
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ViewPagerMembrosFragment.newInstance("isIOS")
            1 -> ViewPagerMembrosFragment.newInstance("isOnline")
            else -> {
                return ViewPagerMembrosFragment.newInstance("Todos")
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "iOS"
            1 -> "Online"
            else -> {
                return "Todos"
            }
        }
    }
}