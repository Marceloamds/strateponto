package com.engefour.strateponto.viewpager

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.engefour.strateponto.R
import com.google.firebase.firestore.FirebaseFirestore
import com.bumptech.glide.Glide
import com.crashlytics.android.Crashlytics
import com.engefour.strateponto.activities.EditarAdminActivity
import com.google.firebase.firestore.Query
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.listview_admin.view.*
import kotlinx.android.synthetic.main.row_gerenciar.view.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*

private const val ARG_PARAM1 = "estadoUsuario"

class ViewPagerAdminFragment: Fragment() {
    private lateinit var estadoUsuario: String
    private var listener: OnFragmentInteractionListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            estadoUsuario = it.getString(ARG_PARAM1)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.listview_admin, container, false)
        val db = FirebaseFirestore.getInstance()
        val adapter = GroupAdapter<ViewHolder>()
        view.listViewAdmin.adapter = adapter

        //Pega as informação de todos os membros. Campo "Todos" do Membros
        if (estadoUsuario == "Membros Ativos") {
            db.collection("usuarios").whereEqualTo("isActive", true)
                .orderBy("userNome", Query.Direction.ASCENDING).addSnapshotListener { documentSnapshot, error ->
                    if (error != null) {
                        Crashlytics.logException(error)
                    } else if (documentSnapshot != null && context != null) {
                        adapter.clear()
                        documentSnapshot.forEach { usuario ->
                            val imageUrl = usuario.get("userFotoUrl").toString()
                            val userNome = usuario.get("userNome").toString()
                            val isActive = usuario.get("isActive") as Boolean
                            val userId = usuario.get("userId").toString()
                            if (activity != null)
                                adapter.add(
                                    UserItem(
                                        imageUrl,
                                        userNome,
                                        userId,
                                        isActive,
                                        activity
                                    )
                                )
                        }
                    }
                }
        }else {
            db.collection("usuarios").whereEqualTo("isActive", false)
                .orderBy("dataInativado", Query.Direction.DESCENDING).addSnapshotListener { documentSnapshot, error ->
                    if (error != null) {
                        Crashlytics.logException(error)
                    } else if (documentSnapshot != null && context != null) {
                        adapter.clear()
                        documentSnapshot.forEach { usuario ->
                            val imageUrl = usuario.get("userFotoUrl").toString()
                            val userNome = usuario.get("userNome").toString()
                            val isActive = usuario.get("isActive") as Boolean
                            val userId = usuario.get("userId").toString()
                            if (activity != null)
                                adapter.add(
                                    UserItem(
                                        imageUrl,
                                        userNome,
                                        userId,
                                        isActive,
                                        activity
                                    )
                                )
                        }
                    }
                }
        }
        //Depois que acaba a query, atualiza a lista
        view.listViewAdmin.layoutManager = LinearLayoutManager(activity)
        view.listViewAdmin.isNestedScrollingEnabled = false
        view.listViewAdmin.isFocusable = false
        return view
    }

    //Classe de UserItem do Groupie
    //Diferente do Leaderboard, que precisa ter um adapter só pra isso por precisar organizar por segundos
    class UserItem(private val imageUrl:String, private val userNome:String,
                   private val userId:String, private val isActive:Boolean, private val activity: Activity?): Item<ViewHolder>(){
        override fun bind(viewHolder: ViewHolder, position: Int) {

            viewHolder.itemView.textViewMembroNome.text = userNome
            Glide.with(viewHolder.itemView).load(imageUrl).into(viewHolder.itemView.imageViewMembroFoto)

            viewHolder.itemView.setOnClickListener {
                val i = Intent(activity, EditarAdminActivity::class.java)
                i.putExtra("userId", userId)
                activity?.startActivity(i)
            }
            if(isActive) {
                viewHolder.itemView.buttonDeletar.background = ContextCompat.getDrawable(activity!!,R.drawable.ic_delete)
                viewHolder.itemView.layoutDeletar.setOnClickListener {
                    val builder = AlertDialog.Builder(activity)
                    builder.setTitle("Desativar Usuário")
                    builder.setMessage("Deseja desativar $userNome?\nO usuário poderá ser reativado futuramente")
                    builder.setPositiveButton("Sim") { _, _ ->
                        val dataInativacao = DateTime.now(DateTimeZone.getDefault()).toString("yy-MM-dd HH.mm.ss", Locale("pt", "BR"))
                        val usuario = HashMap<String, Any>()
                        usuario["isActive"] = false
                        usuario["dataInativado"] = dataInativacao
                        FirebaseFirestore.getInstance().document("usuarios/$userId").update(usuario)
                            .addOnSuccessListener {
                                Toast.makeText(activity,"Usuário Desativado!", Toast.LENGTH_LONG).show()
                            }
                    }
                    builder.setNegativeButton("Cancelar") { _, _ -> }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }else{
                viewHolder.itemView.buttonDeletar.background = ContextCompat.getDrawable(activity!!,R.drawable.ic_undo)
                viewHolder.itemView.layoutDeletar.setOnClickListener {
                    val builder = AlertDialog.Builder(activity)
                    builder.setTitle("Reativar Usuário")
                    builder.setMessage("Deseja Reativar $userNome?\nO usuário poderá ser deletado futuramente")
                    builder.setPositiveButton("Sim") { _, _ ->
                        val usuario = HashMap<String, Any>()
                        usuario["isActive"] = true
                        usuario["dataInativado"] = ""
                        FirebaseFirestore.getInstance().document("usuarios/$userId").update(usuario)
                            .addOnSuccessListener {
                                Toast.makeText(activity,"Usuário Reativado!", Toast.LENGTH_LONG).show()
                            }
                    }
                    builder.setNegativeButton("Cancelar") { _, _ -> }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }
        }

        override fun getLayout(): Int {
            return R.layout.row_gerenciar
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(estadoUsuario: String?) =
            ViewPagerAdminFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, estadoUsuario)
                }
            }
    }
}
