package com.engefour.strateponto.viewpager

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.engefour.strateponto.activities.MainActivity
import com.engefour.strateponto.R
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.listview_leaderboard.view.*
import org.joda.time.DateTime
import android.widget.Toast
import com.google.firebase.firestore.Query
import org.joda.time.DateTimeZone


private const val ARG_PARAM1 = "campoPesquisa"

class ViewPagerLeaderboardFragment : Fragment(){
    private var campoPesquisa: String? = null
    private var listener: OnFragmentInteractionListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            campoPesquisa = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.listview_leaderboard, container, false)
        val db = FirebaseFirestore.getInstance()
        var segundosTotais = 0

        //pra cada usuário, acessa o banco pega os horários somados de acordo com o campoPesquisa
        val adapter = ViewPagerLeaderboardGroupAdapter(activity)
        val listAux: MutableList<UserItem> = mutableListOf()

        db.collection("usuarios/").orderBy("userNome", Query.Direction.DESCENDING)
            .whereEqualTo("isActive",true).get().addOnSuccessListener { usuarios ->
            usuarios.forEach { usuario ->
                val imageUrl = usuario.get("userFotoUrl").toString()
                val userNome = usuario.get("userNome").toString()
                val userId = usuario.get("userId").toString()

                when(campoPesquisa){
                    "semanaPassadaHoras"-> {
                        //Vê se a semanaUltimoPlantão é igual a semana atual ou a anterior
                        //Se for a semana atual, pega semanaPassadaHoras. O usuário está "regular"
                        //Se for a semana anterior, pega SemanaHoras. O usuário não entra desde semana passada
                        //Se fizer mais de duas semanas, zera as horas
                        if(usuario.getLong("semanaUltimoPlantao")?.toInt() == DateTime.now(DateTimeZone.getDefault()).weekOfWeekyear)
                            segundosTotais = usuario.getLong("semanaPassadaHoras")!!.toInt()
                        else if(usuario.getLong("semanaUltimoPlantao")?.toInt() == DateTime.now(DateTimeZone.getDefault()).weekOfWeekyear -1 )
                                segundosTotais = usuario.getLong("semanaHoras")!!.toInt()
                            else
                                segundosTotais = 0
                    }
                    "semanaHoras"-> {
                        //Se for dessa semana pega SemanaHoras, se não zera
                        if(usuario.getLong("semanaUltimoPlantao")?.toInt() == DateTime.now(DateTimeZone.getDefault()).weekOfWeekyear().get())
                            segundosTotais = usuario.getLong("semanaHoras")!!.toInt()
                        else
                            segundosTotais = 0
                    }
                    "mesHoras"-> {
                        //Se for desse mês pega mesHoras, se não zera
                        if(usuario.getLong("mesUltimoPlantao")?.toInt() == DateTime.now(DateTimeZone.getDefault()).monthOfYear)
                            segundosTotais = usuario.getLong("mesHoras")!!.toInt()
                        else
                            segundosTotais = 0
                    }
                    "mesPassadoHoras"-> {
                        //Vê se a mesUltimoPlantão é igual a semana atual ou a anterior
                        //Se for a mes atual, pega mesPassadoHoras. O usuário está "regular"
                        //Se for a mes anterior, pega mesHoras. O usuário não entra desde mes passado
                        //Se fizer mais de dois meses, zera as horas
                        if(usuario.getLong("mesUltimoPlantao")?.toInt() == DateTime.now(DateTimeZone.getDefault()).monthOfYear().get())
                            segundosTotais = usuario.getLong("mesPassadoHoras")!!.toInt()
                        else if(usuario.getLong("mesUltimoPlantao")?.toInt() == DateTime.now(DateTimeZone.getDefault()).monthOfYear().get() -1 )
                            segundosTotais = usuario.getLong("mesHoras")!!.toInt()
                            else
                                segundosTotais = 0
                    }
                    "anoHoras"-> {
                        //Se for desse ano, pega anoHoras, se não zera
                        if(usuario.getLong("anoUltimoPlantao")?.toInt() == DateTime.now(DateTimeZone.getDefault()).year)
                            segundosTotais = usuario.getLong("anoHoras")!!.toInt()
                        else
                            segundosTotais = 0
                    }
                }

                // Faz o cálculo de horas, minutos e segundos de acordo com os segundos
                val horas:Int = segundosTotais/3600
                val minutos:Int = (segundosTotais - horas*3600) /60
                val segundos = segundosTotais%60

                //Formata no estilo "02h30min02s"
                val tempo = String.format("%02dh%02dmin%02ds", horas, minutos, segundos)

                listAux.add(
                    UserItem(
                        imageUrl,
                        userNome,
                        tempo,
                        segundosTotais,
                        userId
                    )
                )
                segundosTotais = 0

                if (listAux.size == usuarios.size()){
                            val main = activity as MainActivity?
                            main?.atualizarListaLeaderboard()
                            adapter.addAll(listAux)
                    }
                }
        }
            .addOnFailureListener{
                Toast.makeText(activity,"Falha ao conectar:" + it.message+ ". Por favor, reenicie o aplicativo.", Toast.LENGTH_LONG)
                    .show()
            }

        //Depois que acaba a query, atualiza a lista

        view.listViewLeaderboard.layoutManager = LinearLayoutManager(activity)
        view.listViewLeaderboard.adapter = adapter
        return view
    }

    //Classe UserItem usado no adapter customizado
    class UserItem(private val imageUrl:String, private val userNome:String, private val tempoTotal:String,private val segundosTotais:Int,private val userID:String){
        fun getTempo():Int {
            return this.segundosTotais
        }
        fun getImage():String {
            return this.imageUrl
        }
        fun getName():String{
            return this.userNome
        }
        fun getHoras():String{
            return this.tempoTotal
        }
        fun getId():String{
            return this.userID
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(campoPesquisa: String) =
            ViewPagerLeaderboardFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, campoPesquisa)
                }
            }
    }
}
