package com.engefour.strateponto.viewpager;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;
import com.bumptech.glide.Glide;
import com.engefour.strateponto.R;
import com.engefour.strateponto.activities.UsuarioActivity;
import org.jetbrains.annotations.NotNull;

import java.util.List;

// Um adapter que ordena os itens que são adicionados automaticamente de acordo com os segundosTotais
// Usado na ListView do LeaderBoard. Copiei a maioria do código online
// Usei pq não dá pra pegar ela em ordem direto do servidor
// Link usado: https://medium.com/@abduazizkayumov/sortedlist-with-recyclerview-c0bf2e31565e
public class ViewPagerLeaderboardGroupAdapter extends RecyclerView.Adapter<ViewPagerLeaderboardGroupAdapter.ViewHolder> {

    private SortedList<ViewPagerLeaderboardFragment.UserItem> list;
    private Context context;
    public ViewPagerLeaderboardGroupAdapter(Context context) {
        this.context = context;
        //Nessa função ele faz a lista ficar em ordem. Com o "compare".
        list = new SortedList<>(ViewPagerLeaderboardFragment.UserItem.class, new SortedList.Callback<ViewPagerLeaderboardFragment.UserItem>() {
            @Override
            public int compare(ViewPagerLeaderboardFragment.UserItem o1, ViewPagerLeaderboardFragment.UserItem o2) {
                Integer o1Tempo = o1.getTempo();
                Integer o2Tempo = o2.getTempo();
                return o2Tempo.compareTo(o1Tempo);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(ViewPagerLeaderboardFragment.UserItem oldItem, ViewPagerLeaderboardFragment.UserItem newItem) {
                return oldItem.getName().equals(newItem.getName());
            }

            @Override
            public boolean areItemsTheSame(ViewPagerLeaderboardFragment.UserItem item1, ViewPagerLeaderboardFragment.UserItem item2) {
                return item1.getName().equals(item2.getName());
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }
        });
    }

    //conversation helpers
    public void addAll(List<ViewPagerLeaderboardFragment.UserItem> usuarios) {
        //adiciona vários itens e colocam eles nos lugares certos.
        list.beginBatchedUpdates();
        for (int i = 0; i < usuarios.size(); i++) {
            list.add(usuarios.get(i));
        }
        list.endBatchedUpdates();
    }

    public ViewPagerLeaderboardFragment.UserItem get(int position) {
        return list.get(position);
    }

    public void clear() {
        list.beginBatchedUpdates();
        //remove items at end, to avoid unnecessary array shifting
        while (list.size() > 0) {
            list.removeItemAt(list.size() - 1);
        }
        list.endBatchedUpdates();
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_leaderboard, parent, false);
        return new ViewHolder(itemView);
    }

    //Aqui popula cada elemento da view
    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        final ViewPagerLeaderboardFragment.UserItem usuario = list.get(position);
        holder.textViewRowHoras.setText(usuario.getHoras());
        holder.textViewRowNome.setText(usuario.getName());
        Glide.with(holder.itemView)
                .load(usuario.getImage())
                .into(holder.imageViewRowFoto);

        holder.textViewPosicao.setText(String.valueOf(position+1));

        if(position == 0) {
            holder.imageViewPremio.setVisibility(View.VISIBLE);
            holder.imageViewPremio.setImageResource(R.drawable.ic_gold);
        }
        else if (position == 1) {
            holder.imageViewPremio.setVisibility(View.VISIBLE);
            holder.imageViewPremio.setImageResource(R.drawable.ic_silver);
        }else if( position == 2) {
            holder.imageViewPremio.setVisibility(View.VISIBLE);
            holder.imageViewPremio.setImageResource(R.drawable.ic_bronze);
        }else{
            holder.imageViewPremio.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, UsuarioActivity.class);
                i.putExtra("userId", usuario.getId());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //ViewHolder. Faz a ponte do layout como adapter
    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewRowNome;
        TextView textViewRowHoras;
        TextView textViewPosicao;
        ImageView imageViewRowFoto;
        ImageView imageViewPremio;

        ViewHolder(View itemView) {
            super(itemView);
            textViewRowNome = itemView.findViewById(R.id.textViewRowNome);
            textViewRowHoras = itemView.findViewById(R.id.textViewRowHoras);
            imageViewRowFoto = itemView.findViewById(R.id.imageViewRowFoto);
            imageViewPremio = itemView.findViewById(R.id.imageViewPremio);
            textViewPosicao = itemView.findViewById(R.id.textViewPosicao);
        }

    }

}