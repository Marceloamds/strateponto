package com.engefour.strateponto.activities

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.bumptech.glide.Glide
import com.engefour.strateponto.viewpager.ViewPagerLeaderboardFragment
import com.engefour.strateponto.viewpager.ViewPagerMembrosFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_leaderboard.*
import kotlinx.android.synthetic.main.fragment_membros.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import android.view.KeyEvent
import com.engefour.strateponto.R
import com.engefour.strateponto.fragments.*
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.content_main.*
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat




class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,

        //Funções obrigatórias para Fragments dentro da MainActivity
        PontoFragment.OnFragmentInteractionListener,
        LeaderboardFragment.OnFragmentInteractionListener,
        MembrosFragment.OnFragmentInteractionListener,
        FeedFragment.OnFragmentInteractionListener,
        AdminFragment.OnFragmentInteractionListener,
        ViewPagerLeaderboardFragment.OnFragmentInteractionListener,
        ViewPagerMembrosFragment.OnFragmentInteractionListener,
    //Funções que usam MainActivity como "ponte" para outros fragments
        LeaderboardFragment.AtualizarListaLeaderboard,
        MembrosFragment.AtualizarListaMembros {

    private lateinit var pontoFragment: PontoFragment
    private lateinit var leaderboardFragment: LeaderboardFragment
    private lateinit var membrosFragment: MembrosFragment
    private lateinit var feedFragment: FeedFragment
    private lateinit var adminFragment: AdminFragment
    private lateinit var usuario: User
    private var isPermited = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseMessaging.getInstance().subscribeToTopic("all")
        retrieveUserInfo()
        checkPermission()

        drawer_toggle.setOnClickListener {
            drawer_layout.openDrawer(GravityCompat.START)
        }

        nav_view.setNavigationItemSelectedListener(this)
        leaderboardFragment = LeaderboardFragment.newInstance()
        pontoFragment = PontoFragment.newInstance()
        membrosFragment = MembrosFragment.newInstance()
        feedFragment = FeedFragment.newInstance()
        adminFragment = AdminFragment.newInstance()
        supportFragmentManager.beginTransaction().replace(R.id.container, pontoFragment).commit()
        nav_view.setCheckedItem(R.id.nav_ponto)

    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this@MainActivity, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(
                    this@MainActivity, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    123)
            }
        } else {
            isPermited = true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            123 -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermited = true

                } else {
                    Toast.makeText(applicationContext, "Please allow the permission", Toast.LENGTH_LONG).show()

                }
            }
        }
    }

    // Vê se o usuário não está logado. Se tiver, pega as informações. Se não, vai pra tela de Login
    private fun retrieveUserInfo() {
        val uid = FirebaseAuth.getInstance().uid
        if (uid == null) { // Se não estiver logado
            val i = Intent(this, LoginActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(i)
        } else { // Se estiver
            val db = FirebaseFirestore.getInstance()
            db.document("usuarios/$uid/").get().addOnSuccessListener {
                usuario = User(
                    it.getString("userNome"),
                    it.getString("userFotoUrl"),
                    it.getString("userCargo"),
                    it.getLong("anoHoras")?.toInt()
                )
                if(it.getBoolean("isActive") != true){

                    val i = Intent(this, LoginActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(i)
                    Toast.makeText(this,"Usuário desativado! Fale com um administrador", Toast.LENGTH_SHORT)
                        .show()
                }

                if(usuario.getUserCargo() != "admin"){
                    nav_view.menu.findItem(R.id.nav_admin).isVisible = false
                }
                populaNavHeader()
            }
                .addOnFailureListener{
                    Toast.makeText(this,"Falha ao conectar: " + it.message, Toast.LENGTH_SHORT)
                        .show()
                }
        }
    }

    // Coloca as informações do usuario no NavHeader
    private fun populaNavHeader(){
        val header = nav_view.getHeaderView(0)
        val uid = FirebaseAuth.getInstance().uid ?: ""

        val horaAno = (usuario.getAnoHoras()?.div(3600))
        header.navUserTitulo.text = horasParaTitulo(horaAno)

        header.nav_user_name.text = usuario.getUserNome()
        if(!(isDestroyed || isFinishing ))
            Glide.with(this).load(usuario.getUserFotoUrl()).into(header.nav_user_photo)

        header.setOnClickListener{
            val i = Intent(this, UsuarioActivity()::class.java)
            i.putExtra("userId",uid)
            startActivity(i)
        }
    }

    //Função chamada pra atualizar a Leaderboard de Horários de plantão
    //É uma interface do Relatório Fragment Chamado Pelo ViewPager usando a MainActivity como "ponte"
    override fun atualizarListaLeaderboard() {
            leaderboardFragment = supportFragmentManager.findFragmentByTag("leaderboard") as LeaderboardFragment
            if(leaderboardFragment.isVisible) {
                leaderboardFragment.tabsLeaderboard.setupWithViewPager(viewPagerLeaderboard)
                leaderboardFragment.loadingDialog.hideDialog()
            }
    }

    //Mesmo esquema da função acima
    override fun atualizarListaMembros() {
            membrosFragment = supportFragmentManager.findFragmentByTag("membros") as MembrosFragment
            if(membrosFragment.isVisible) {
                membrosFragment.tabsMembros.setupWithViewPager(viewPagerMembros)
                membrosFragment.loadingDialog.hideDialog()
            }
    }



    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    //Troca o fragment de acordo com o item selecionado no NavigationView
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_ponto -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container,pontoFragment,"ponto")
                    .addToBackStack(pontoFragment.toString())
                    .setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.nav_leaderboard -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container,leaderboardFragment,"leaderboard")
                    .addToBackStack(leaderboardFragment.toString())
                    .setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.nav_membros -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container,membrosFragment,"membros")
                    .addToBackStack(membrosFragment.toString())
                    .setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.nav_feed -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container,feedFragment,"feed")
                    .addToBackStack(FeedFragment.toString())
                    .setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.nav_admin ->{
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container,adminFragment,"admin")
                    .addToBackStack(AdminFragment.toString())
                    .setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.nav_signout -> {
                //Se clicar no "Sair" manda um AlertDialog pra confirmar
                val builder = AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.quit))
                builder.setMessage(getString(R.string.really_quit))
                builder.setPositiveButton("Sim") { _, _ ->
                    FirebaseAuth.getInstance().signOut()
                    val i = Intent(this, LoginActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(i)
                }
                builder.setNegativeButton("Cancelar") { _, _ -> }
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onFragmentInteraction(uri: Uri) {}


    //Se apertar pra voltar e não tiver nenhum fragment na backstack
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        //Handle the back button
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
            return true
        } else {
            return if (keyCode == KeyEvent.KEYCODE_BACK && supportFragmentManager.backStackEntryCount == 0) {
                //Ask the user if they want to quit
                AlertDialog.Builder(this)
                    .setTitle(getString(R.string.quit))
                    .setMessage(getString(R.string.really_quit))
                    .setPositiveButton("Sim") { _, _ ->
                        //Stop the activity
                        finish()
                    }
                    .setNegativeButton("Cancelar", null)
                    .show()
                true
            } else {
                super.onKeyDown(keyCode, event)
            }
        }
    }
    private fun horasParaTitulo(horas:Int?):String{
        when (horas) {
            //Coloca o título de acordo com as horas anuais da pessoa
            in 0..10 -> return getString(R.string.titulo_0a10)
            in 11..20 -> return getString(R.string.titulo_11a20)
            in 21..50 -> return getString(R.string.titulo_21a50)
            in 51..100 -> return  getString(R.string.titulo_51a100)
            in 101..150 -> return getString(R.string.titulo_101a150)
            in 151..200 -> return getString(R.string.titulo_151a200)
            in 201..250 -> return getString(R.string.titulo_201a250)
            in 251..300 -> return getString(R.string.titulo_251a300)
            in 301..400 -> return getString(R.string.titulo_301a400)
            in 401..500 -> return getString(R.string.titulo_401a500)
            in 501..600 -> return getString(R.string.titulo_501a600)
            in 601..700 -> return getString(R.string.titulo_601a700)
            else -> return getString(R.string.titulo_601a700)
        }
    }

}

class  User(private val userNome: String?, private val userFotoUrl: String?, private val userCargo: String?, private val anoHoras:Int?){
    constructor() : this( "","","",0)

    fun getUserNome():String?{
        return userNome
    }
    fun getUserFotoUrl():String?{
        return userFotoUrl
    }
    fun getUserCargo():String?{
        return userCargo
    }
    fun getAnoHoras():Int?{
        return anoHoras
    }
}