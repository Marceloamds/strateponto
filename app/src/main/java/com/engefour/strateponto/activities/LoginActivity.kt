package com.engefour.strateponto.activities

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.engefour.strateponto.R
import com.engefour.strateponto.uteis.LoadingDialog
import com.google.firebase.auth.FirebaseAuth
import  kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        lateinit var loadingDialog: LoadingDialog

        //Ao clicar entrar, chama o Firebase Auth para validar os dados
        entrar_button.setOnClickListener {
            loadingDialog = LoadingDialog(this)
            loadingDialog.showDialog()
            val email = editTextSenhaAtual.text.toString()
            val senha = editTextSenha.text.toString()

            if (email.isEmpty() || senha.isEmpty()) {
                Toast.makeText(this, getString(R.string.login_empty), Toast.LENGTH_SHORT).show()
                loadingDialog.hideDialog()
            } else{
                FirebaseAuth.getInstance().signInWithEmailAndPassword(email, senha)
                    .addOnSuccessListener {
                        loadingDialog.hideDialog()
                        val i = Intent(this, MainActivity::class.java)
                        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(i)
                    }
                    //Se falhar o login
                    .addOnFailureListener {
                        loadingDialog.hideDialog()
                        Toast.makeText(this, "Falha ao fazer login: " + it.message, Toast.LENGTH_SHORT)
                            .show()
                    }
            }
        }
        esqueceuSenhaButton.setOnClickListener{
            //Chama o popup de troca e acessa os elementos
            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCanceledOnTouchOutside(true)
            val view = LayoutInflater.from(this).inflate(R.layout.popup_recuperar_senha,null)
            val editTextEmail = view.findViewById<EditText>(R.id.editTextEmail)
            val mudarSenhaButton = view.findViewById<Button>(R.id.mudarSenhaButton)
            mudarSenhaButton.setOnClickListener{
                if(editTextEmail.text.isEmpty())
                    Toast.makeText(this, "Preencha o e-mail ", Toast.LENGTH_SHORT).show()
                else {
                    //Envia o e-mail de troca de senha
                    FirebaseAuth.getInstance().sendPasswordResetEmail(editTextEmail.text.toString()).addOnSuccessListener {
                        Toast.makeText(this, "E-mail enviado! Veja a caixa de Spam", Toast.LENGTH_LONG).show()
                    }.addOnFailureListener{
                        Toast.makeText(this, "Falha no envio do e-mail: "+it.message, Toast.LENGTH_LONG).show()
                    }

                }
            }
            dialog.setContentView(view)
            dialog.show()
        }

        registrar_button.setOnClickListener{
            val i = Intent(this, RegisterActivity::class.java)
            startActivity(i)
        }
    }
}
