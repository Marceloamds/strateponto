package com.engefour.strateponto.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.engefour.strateponto.R
import com.engefour.strateponto.uteis.LoadingDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_usuario.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat
import java.util.*

class UsuarioActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_usuario)

        val db = FirebaseFirestore.getInstance()
        val uid = intent.getStringExtra("userId")

        //Se estiver visitando um perfil que não é do próprio usuário, some com o botão de edição
        if(uid == FirebaseAuth.getInstance().uid) {
            buttonEditar.setOnClickListener {
                val i = Intent(this, EditarUsuarioActivity::class.java)
                startActivity(i)
            }
        }
        else{
            buttonEditar.visibility = View.GONE
            buttonEditar.isClickable = false
        }

        backButton.setOnClickListener{
            onBackPressed()
        }

        val loadingDialog = LoadingDialog(this)
        loadingDialog.showDialog()
        loadingDialog.desativarBotao()

        //pega as informações do usuário
        db.document("usuarios/$uid").get().addOnSuccessListener {

            val imageUrl = it.get("userFotoUrl")
            if(!(isDestroyed || isFinishing ))
                Glide.with(this).load(imageUrl).into(imageViewFoto)

            val nome = it.get("userNome").toString()
            textViewNome.text = nome
            userApelido.text = getString(R.string.usuario_apelido, it.get("userApelido").toString())

            //Pega os segundos de cada campo de divide por 3600 pra conseguir as horas
            val horaAno = (it.getLong("anoHoras")?.div(3600))?.toInt()
            if (it.getLong("semanaUltimoPlantao")?.toInt() == DateTime.now().weekOfWeekyear().get())
                textViewHorasSemanais.text = (it.getLong("semanaHoras")?.div(3600)).toString()

                textViewHorasMensais.text = (it.getLong("mesHoras")?.div(3600)).toString()
            if (it.get("anoUltimoPlantao").toString().toInt() == DateTime.now().year().get())
                textViewHorasAnuais.text = horaAno.toString()

            textViewTitulo.text = horasParaTitulo(horaAno)

//                buttonAdmin.visibility = View.GONE
//                buttonAdmin.isClickable = false
//                val constraintSet = ConstraintSet()
//                constraintSet.clone(usuarioLayout)
//                constraintSet.connect(textViewNome.id,ConstraintSet.TOP,imageViewFoto.id,ConstraintSet.BOTTOM)
//                constraintSet.applyTo(usuarioLayout)



            lateinit var horarioEntrada: String
            var dateEntrada: DateTime? = null

            // Se o usuário nunca marcou um plantão
            if (it.get("horaEntrada") != null) {
                horarioEntrada = it.get("horaEntrada").toString()
                dateEntrada = DateTime.parse(
                    horarioEntrada,
                    DateTimeFormat.forPattern("dd-MM-yy hh.mm.ss aa").withLocale(Locale("pt", "BR"))
                )
            }

            //Pega o mês que estamos e o ano pra colocar nas TextViews
            val sdf = SimpleDateFormat("MMMM", Locale("pt", "BR"))
            val d = Date()
            var monthOfYear = sdf.format(d).capitalize()
            textViewMensal.text = getString(R.string.hora_mensal, monthOfYear)
            textViewAnual.text = getString(R.string.hora_anual, DateTime.now().year)

            if (dateEntrada != null) {
                monthOfYear = sdf.format(dateEntrada.toDate()).capitalize()
                val dataEntrada = String.format(
                    "%02d de $monthOfYear as %02dh%02d",
                    dateEntrada.dayOfMonth,
                    dateEntrada.hourOfDay,
                    dateEntrada.minuteOfHour
                )
                textViewUltimaEntrada.text = getString(R.string.visto_por_ultimo, dataEntrada)
            }
            else
                textViewUltimaEntrada.text = getString(R.string.nenhum_plantao)

            loadingDialog.hideDialog()
        }.addOnFailureListener {
            Toast.makeText(
                this,
                "Falha ao conectar:" + it.message + ". Por favor, reenicie o aplicativo.",
                Toast.LENGTH_LONG
            )
                .show()
        }
    }

    override fun onBackPressed() {
        if (isTaskRoot) {
            intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        } else {
            finish()
        }
    }

    private fun horasParaTitulo(horas:Int?):String{
        when (horas) {
            //Coloca o título de acordo com as horas anuais da pessoa
            in 0..10 -> return getString(R.string.titulo_0a10)
            in 11..20 -> return getString(R.string.titulo_11a20)
            in 21..50 -> return getString(R.string.titulo_21a50)
            in 51..100 -> return  getString(R.string.titulo_51a100)
            in 101..150 -> return getString(R.string.titulo_101a150)
            in 151..200 -> return getString(R.string.titulo_151a200)
            in 201..250 -> return getString(R.string.titulo_201a250)
            in 251..300 -> return getString(R.string.titulo_251a300)
            in 301..400 -> return getString(R.string.titulo_301a400)
            in 401..500 -> return getString(R.string.titulo_401a500)
            in 501..600 -> return getString(R.string.titulo_501a600)
            in 601..700 -> return getString(R.string.titulo_601a700)
            else -> return getString(R.string.titulo_601a700)
        }
    }
}

    //Se ele acabou de voltar de uma edição de usuário(Ele vira Root daí), ele vai pra MainActivity
    //Caso contrário ele só dá finish
//    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
//        //Handle the back button
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//
