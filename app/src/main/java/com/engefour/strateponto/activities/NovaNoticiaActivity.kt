package com.engefour.strateponto.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.engefour.strateponto.uteis.LoadingDialog
import kotlinx.android.synthetic.main.activity_nova_noticia.*
import android.provider.OpenableColumns
import com.engefour.strateponto.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import org.joda.time.DateTime
import java.util.*


class NovaNoticiaActivity : AppCompatActivity() {

    private lateinit var loadingDialog: LoadingDialog
    private var selectedPdf: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nova_noticia)

        noticiaButton.setOnClickListener{
            loadingDialog = LoadingDialog(this)
            loadingDialog.showDialog()
            loadingDialog.desativarBotao()
            val titulo = editTextTitulo.text.toString()
            val descricao = editTextDescricao.text.toString()

            if(titulo.isEmpty() || descricao.isEmpty()){
                Toast.makeText(this,"Preencha todos os campos", Toast.LENGTH_SHORT).show()
                loadingDialog.hideDialog()
            }
            else{
                val filename = UUID.randomUUID().toString()
                val uid = FirebaseAuth.getInstance().currentUser?.uid
                if (uid != "")
                    if(selectedPdf!= null) {
                        loadingDialog.changeText("Upando o PDF")
                        val ref = FirebaseStorage.getInstance().getReference("documents/$filename")
                        ref.putFile(selectedPdf!!).addOnSuccessListener {
                            ref.downloadUrl.addOnSuccessListener {uri->
                                salvarNoticiaDatabase(uri.toString())
                            }
                        }
                    } else
                        salvarNoticiaDatabase("")
            }
        }

        backButton.setOnClickListener{
            finish()
        }

        anexoButton.setOnClickListener{
            val intent= Intent(Intent.ACTION_GET_CONTENT)
            intent.type= "application/pdf"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(intent,1)
        }
    }



    private fun salvarNoticiaDatabase(pdfUrl:String){
        loadingDialog.changeText("Salvando noticia")
        val filename = UUID.randomUUID().toString()
        val db = FirebaseFirestore.getInstance()
        val noticia = HashMap<String, Any>()
        val uid = FirebaseAuth.getInstance().currentUser?.uid
        val titulo = editTextTitulo.text.toString()
        val descricao = editTextDescricao.text.toString()
        db.document("usuarios/$uid").get().addOnSuccessListener {usuario ->
            val horaCompleta = DateTime.now().toString("yy-MM-dd HH.mm.ss", Locale("pt", "BR"))

            noticia["noticiaAnexo"] = pdfUrl
            noticia["noticiaHora"] = horaCompleta
            noticia["noticiaId"] = filename
            noticia["noticiaTitulo"] = titulo
            noticia["noticiaDescricao"] = descricao
            noticia["userId"] = uid!!
            noticia["userFotoUrl"] = usuario.get("userFotoUrl").toString()
            noticia["userApelido"] = usuario.get("userNome").toString()
            if(selectedPdf!=null)
                 noticia["nomeArquivo"] = textViewAnexo.text.toString()

            db.collection("feed").document(filename).set(noticia)
                .addOnSuccessListener {
                    loadingDialog.hideDialog()
                    finish()
                    Toast.makeText(this, "Noticia Criada!!", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener{
                    loadingDialog.hideDialog()
                    Toast.makeText(this,"Falha ao registrar notícia: " + it.message, Toast.LENGTH_LONG).show()
                }
            }
            .addOnFailureListener{
                loadingDialog.hideDialog()
                Toast.makeText(this,"Falha upar o documento: " + it.message, Toast.LENGTH_LONG).show()
         }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data!= null) {
                // Get the Uri of the selected file
                selectedPdf = data.data
                if(selectedPdf != null)
                    textViewAnexo.text = getFileName(selectedPdf!!)
        }
    }


        private fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst())
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1)
                result = result.substring(cut + 1)
        }
        return result
    }

}
