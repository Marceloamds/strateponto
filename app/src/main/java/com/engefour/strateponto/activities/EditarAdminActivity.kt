package com.engefour.strateponto.activities
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.widget.Toast
import com.bumptech.glide.Glide
import com.engefour.strateponto.uteis.LoadingDialog
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_editar_admin.*
import java.util.*
import android.text.TextWatcher
import com.engefour.strateponto.R


class EditarAdminActivity : AppCompatActivity() {

    //mudouFoto vai pra true só se a pessoa trocou de foto
    //Dar upload na foto demora e gasta espaço. Impede que a pessoa só clique no "ok"
    private var mudouFoto = false
    private lateinit var loadingDialog: LoadingDialog
    private var uid: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editar_admin)

        loadingDialog = LoadingDialog(this)
        loadingDialog.showDialog()
        loadingDialog.desativarBotao()

        val db = FirebaseFirestore.getInstance()
        uid = intent.getStringExtra("userId")

        //Pega os dados do usuário e popula eles dentro dos editText pra que eles possam ser editados
        db.document("usuarios/$uid").get().addOnSuccessListener {
            val imageUrl = it.get("userFotoUrl").toString()
            val userNome = it.get("userNome").toString()
            val userApelido = it.get("userApelido").toString()
            val cargoUsuario = it.get("userCargo").toString()
            val isIOS = it.getBoolean("isIOS")
            val semanaHoras = it.getLong("semanaHoras")!!.toInt()
            val mesHoras = it.getLong("mesHoras")!!.toInt()
            val anoHoras = it.getLong("anoHoras")!!.toInt()
            val semanaPassadaHoras = it.getLong("semanaPassadaHoras")!!.toInt()
            val mesPassadoHoras = it.getLong("mesPassadoHoras")!!.toInt()

            editTextTempoSemana.setText(semanaHoras.toString())
            textViewTempoSemana.text = segundosParaHoras(semanaHoras)
            editTextTempoSemana.addTextChangedListener(object: TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    if (p0.toString() == "")
                        textViewTempoSemana.text = segundosParaHoras(0)
                    else
                        textViewTempoSemana.text = segundosParaHoras(editTextTempoSemana.text.toString().toInt())
                }})

            editTextTempoMes.setText(mesHoras.toString())
            textViewTempoMes.text = segundosParaHoras(mesHoras)
            editTextTempoMes.addTextChangedListener(object: TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    if (p0.toString() == "")
                        textViewTempoMes.text = segundosParaHoras(0)
                    else
                        textViewTempoMes.text = segundosParaHoras(editTextTempoMes.text.toString().toInt())
            }})

            editTextTempoAno.setText(anoHoras.toString())
            textViewTempoAno.text = segundosParaHoras(anoHoras)
            editTextTempoAno.addTextChangedListener(object: TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    if (p0.toString() == "")
                        textViewTempoAno.text = segundosParaHoras(0)
                    else
                        textViewTempoAno.text = segundosParaHoras(editTextTempoAno.text.toString().toInt())
            }})

            editTextSemanaPassada.setText(semanaPassadaHoras.toString())
            textViewSemanaPassada.text = segundosParaHoras(semanaPassadaHoras)
            editTextSemanaPassada.addTextChangedListener(object: TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    if (p0.toString() == "")
                        textViewSemanaPassada.text = segundosParaHoras(0)
                    else
                        textViewSemanaPassada.text = segundosParaHoras(p0.toString().toInt())
            }})

            editTextMesPassado.setText(mesPassadoHoras.toString())
            textViewMesPassado.text = segundosParaHoras(mesPassadoHoras)
            editTextMesPassado.addTextChangedListener(object: TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    if (p0.toString() == "")
                        textViewMesPassado.text = segundosParaHoras(0)
                    else
                        textViewMesPassado.text = segundosParaHoras(p0.toString().toInt())
                }})

            if(cargoUsuario == "admin"){
                buttonAdmin.text = getString(R.string.remover_administrador)
                buttonAdmin.setOnClickListener{
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Mudar estado")
                    builder.setMessage("Deseja remover o administrador de $userNome?")
                    builder.setPositiveButton("Sim") { _,_ ->
                        loadingDialog.showDialog()
                        val usuarioCargo = HashMap<String, Any>()
                        usuarioCargo["userCargo"] = ""
                        FirebaseFirestore.getInstance().document("usuarios/$uid")
                            .update(usuarioCargo).addOnSuccessListener {
                                loadingDialog.hideDialog()
                                Toast.makeText(this,"Usuario atualizado!", Toast.LENGTH_LONG).show()
                                val i = Intent(this, EditarAdminActivity::class.java)
                                i.putExtra("userId",uid)
                                startActivity(i)
                                finish()
                            }.addOnFailureListener{error ->
                                Toast.makeText(this,"Falha ao conectar:" +error.message+ ". Por favor, reenicie o aplicativo.", Toast.LENGTH_LONG)
                                    .show()
                            }
                    }
                    builder.setNegativeButton("Cancelar") { _,_ ->}
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }

            }else {
                buttonAdmin.text = getString(R.string.tornar_administrador)
                buttonAdmin.setOnClickListener{
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Mudar estado")
                    builder.setMessage("Deseja tornar $userNome um administrador?")
                    builder.setPositiveButton("Sim") { _,_ ->
                        loadingDialog.showDialog()
                        val usuarioCargo = HashMap<String, Any>()
                        usuarioCargo["userCargo"] = "admin"
                        FirebaseFirestore.getInstance().document("usuarios/$uid")
                            .update(usuarioCargo).addOnSuccessListener {
                                loadingDialog.hideDialog()
                                Toast.makeText(this,"Usuario atualizado!", Toast.LENGTH_LONG).show()
                                val i = Intent(this, EditarAdminActivity::class.java)
                                i.putExtra("userId",uid)
                                startActivity(i)
                                finish()
                            }.addOnFailureListener{error ->
                                Toast.makeText(this,"Falha ao conectar:" +error.message+ ". Por favor, reenicie o aplicativo.", Toast.LENGTH_LONG)
                                    .show()
                            }
                    }
                    builder.setNegativeButton("Cancelar") { _,_ ->}
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }

            }



        editTextMesPassado.setText(mesPassadoHoras.toString())
            textViewMesPassado.text = segundosParaHoras(mesPassadoHoras)
            textViewMesPassado.addTextChangedListener(object: TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    if (p0.toString() == "")
                        textViewMesPassado.text = segundosParaHoras(0)
                    else
                        textViewMesPassado.text = segundosParaHoras(textViewMesPassado.text.toString().toInt())
                }})

            if(!(isDestroyed || isFinishing ))
                Glide.with(this).load(imageUrl).into(imageViewFoto)

            editTextNome.setText(userNome)
            editTextApelido.setText(userApelido)

            if(isIOS == true)
                spinnerOS.setItems("iOS", "Android")
            else
                spinnerOS.setItems("Android", "iOS")

            loadingDialog.hideDialog()
        }.addOnFailureListener{
            Toast.makeText(this,"Falha ao conectar:" + it.message+ ". Por favor, reenicie o aplicativo.", Toast.LENGTH_LONG)
                .show()
        }

        //Inicializa a galeria do android
        buttonAlterarFoto.setOnClickListener{
            val intent= Intent(Intent.ACTION_PICK)
            intent.type= "image/*"
            startActivityForResult(intent,0)
        }

        backButton.setOnClickListener{
            onBackPressed()
        }

        buttonConfirma.setOnClickListener{
            loadingDialog = LoadingDialog(this)
            loadingDialog.showDialog()

            if(mudouFoto)
                uploadImageToFirebaseStorage()

            val usuario = HashMap<String, Any>()

            if(editTextTempoSemana.text.isEmpty())
                editTextTempoSemana.setText("0")
            if(editTextTempoMes.text.isEmpty())
                editTextTempoMes.setText("0")
            if(editTextTempoAno.text.isEmpty())
                editTextTempoAno.setText("0")
            if(editTextSemanaPassada.text.isEmpty())
                editTextSemanaPassada.setText("0")
            if(editTextMesPassado.text.isEmpty())
                editTextMesPassado.setText("0")

            usuario["userNome"] = editTextNome.text.toString()
            usuario["userApelido"] = editTextApelido.text.toString()
            usuario["semanaHoras"] = editTextTempoSemana.text.toString().toInt()
            usuario["mesHoras"] = editTextTempoMes.text.toString().toInt()
            usuario["anoHoras"] = editTextTempoAno.text.toString().toInt()
            usuario["semanaPassadaHoras"] = editTextSemanaPassada.text.toString().toInt()
            usuario["mesPassadoHoras"] = editTextMesPassado.text.toString().toInt()
            usuario["isIOS"] = (spinnerOS.text.toString() == "iOS")

            db.document("usuarios/$uid")
                .update(usuario).addOnSuccessListener {
                    loadingDialog.hideDialog()
                    Toast.makeText(this,"Usuario atualizado!", Toast.LENGTH_SHORT).show()
                    val i = Intent(this, GerenciarActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(i)
                }
                .addOnFailureListener{
                    loadingDialog.hideDialog()
                    Toast.makeText(this,"Falha ao atualizar usuario: " + it.message, Toast.LENGTH_SHORT).show()
                }

        }

    }

    private var selectedPhotoUri: Uri? = null

    //Quando o usuário seleciona a imagem
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data!= null){
            selectedPhotoUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver,selectedPhotoUri)
            imageViewFoto.setImageBitmap(bitmap)
            mudouFoto = true
        }
    }
    private fun segundosParaHoras(tempo:Int):String{
        val horas:Int = tempo/3600
        val minutos:Int = (tempo - horas*3600) /60
        val segundos = tempo%60
        return String.format("%02dh%02dmin%02ds", horas, minutos, segundos)
    }

    // Manda a imagem pro Firebase Storage
    private fun  uploadImageToFirebaseStorage() {
        val filename = UUID.randomUUID().toString()
        val ref =  FirebaseStorage.getInstance().getReference("image/$filename")
        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener { uri->
                    val usuarioHorario = HashMap<String, Any>()
                    usuarioHorario["userFotoUrl"] = uri.toString()
                    FirebaseFirestore.getInstance().document("usuarios/$uid")
                        .update(usuarioHorario)
                    //  uri.toString() = link para download
                }
                    .addOnFailureListener{
                        loadingDialog.hideDialog()
                        Toast.makeText(this,"Falha ao registrar foto: " + it.message, Toast.LENGTH_SHORT).show()
                    }
            }

    }
}
