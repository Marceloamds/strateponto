package com.engefour.strateponto.activities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.engefour.strateponto.R
import com.engefour.strateponto.viewpager.ViewPagerGerenciarAdapter
import com.engefour.strateponto.viewpager.ViewPagerAdminFragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_gerenciar.*

class GerenciarActivity : AppCompatActivity(), ViewPagerAdminFragment.OnFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gerenciar)

        val fragmentAdapter = ViewPagerGerenciarAdapter(supportFragmentManager)
        viewPagerAdmin.isSaveFromParentEnabled = true
        viewPagerAdmin.isSaveEnabled = true
        viewPagerAdmin.adapter = fragmentAdapter


        tabsAdmin.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewPagerAdmin))
        viewPagerAdmin.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabsAdmin))

        backButton.setOnClickListener{
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        if (isTaskRoot) {
            intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        } else {
            finish()
        }
    }
    override fun onFragmentInteraction(uri: Uri) {}
}
