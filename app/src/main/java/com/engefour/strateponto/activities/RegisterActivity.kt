package com.engefour.strateponto.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import com.engefour.strateponto.R
import com.engefour.strateponto.uteis.LoadingDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage

import kotlinx.android.synthetic.main.activity_register.*
import java.util.*

class RegisterActivity : AppCompatActivity() {
    private lateinit var loadingDialog: LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        spinnerOS.setItems("Android", "iOS")
        spinnerOS.hint = "Selecione o Sistema"

        registrar_button.setOnClickListener{
            registrar()
        }


        login_button.setOnClickListener {
            val i = Intent(this, LoginActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(i)
        }

        //Chama o picker de imagem nativo do android
        select_photo.setOnClickListener{
            val intent= Intent(Intent.ACTION_PICK)
            intent.type= "image/*"
            startActivityForResult(intent,0)
        }
    }

    private var selectedPhotoUri: Uri? = null

    //Quando ele seleciona a imagem
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data!= null){
            selectedPhotoUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver,selectedPhotoUri)
            //Tem dois select_photo. Um botão pra clicar e outro com a imagem
            //Quando seleciona, deixa o botão invisível e mostra a imagem
            imageViewFoto.setImageBitmap(bitmap)
            select_photo.alpha = 0f
        }
    }

    //Registra no Firebase Auth
    private fun registrar(){
        val email = editTextEmail.text.toString()
        val senha = editTextSenha.text.toString()
        val confirmaSenha = editTextConfirmarSenha.text.toString()
        val nome = editTextNome.text.toString()
        val apelido = editTextApelido.text.toString()
        val chaveDeAcesso = editTextChaveDeAcesso.text.toString()
        val sistema = spinnerOS.text.toString()

        if (email.isEmpty() || senha.isEmpty() || nome.isEmpty() || apelido.isEmpty() || chaveDeAcesso.isEmpty() || sistema.isEmpty()) {
            Toast.makeText(this,"Preencha todos os campos",Toast.LENGTH_SHORT).show()
            return
        } else if (selectedPhotoUri == null) {
            Toast.makeText(this,"Escolha uma foto!",Toast.LENGTH_SHORT).show()
            return
        } else if(senha != confirmaSenha){
            Toast.makeText(this,"Senha e confirmação não são iguais",Toast.LENGTH_SHORT).show()
            return
        }else{
            loadingDialog = LoadingDialog(this)
            loadingDialog.showDialog()
            loadingDialog.changeText("Conferindo a chave")
            FirebaseFirestore.getInstance().document("sala/estadoSala").get().addOnSuccessListener {
                if (it.get("chaveDeAcesso")!! != chaveDeAcesso){
                    Toast.makeText(this,"Chave de acesso incorreta!",Toast.LENGTH_SHORT).show()
                    loadingDialog.hideDialog()
                }
                else{
                    //Primeiro upa a imagem. É o que mais leva tempo
                    uploadImageToFirebaseStorage()
                }
            }.addOnFailureListener {
                loadingDialog.hideDialog()
                Toast.makeText(this,"Falha ao conectar: " + it.message, Toast.LENGTH_SHORT).show()
            }
        }

        return
    }

    // Manda a imagem pro Firebase Storage
    private fun uploadImageToFirebaseStorage(){
        loadingDialog.changeText("Upando a imagem")
        loadingDialog.desativarBotao()

        val email = editTextEmail.text.toString()
        val senha = editTextSenha.text.toString()
        val filename = UUID.randomUUID().toString()
        val ref =  FirebaseStorage.getInstance().getReference("image/$filename")
        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                // se conseguiu upar
                //  it.metadata?.path = image/endereço-aleatorio no firebase storage
                ref.downloadUrl.addOnSuccessListener { uri->
                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,senha)
                        .addOnSuccessListener {
                            saveUserToDatabase(uri.toString())
                        }
                        .addOnFailureListener{
                            loadingDialog.hideDialog()
                            Toast.makeText(this,"Falha ao registrar usuario: " + it.message, Toast.LENGTH_SHORT)
                                .show()
                        }
                   //  it.toString() = link para download
                }

            }
            .addOnFailureListener{
                loadingDialog.hideDialog()
                Toast.makeText(this,"Falha ao registrar foto: " + it.message, Toast.LENGTH_SHORT).show()
            }
    }

    // Salva os dados no Firestore
    private fun saveUserToDatabase(profileImageUrl: String){
        loadingDialog.changeText("Registrando usuario")
        val uid = FirebaseAuth.getInstance().uid ?: ""
            val db = FirebaseFirestore.getInstance()
            val user = HashMap<String, Any>()
            user["userId"] = uid
            user["isOnline"] = false
            user["userNome"] = editTextNome.text.toString()
            user["userFotoUrl"] = profileImageUrl
            user["userApelido"] = editTextApelido.text.toString()
            user["isActive"] = true
            user["semanaUltimoPlantao"] = 0
            user["semanaHoras"] = 0
            user["semanaPassadaHoras"] = 0
            user["mesPassadoHoras"] = 0
            user["mesHoras"] = 0
            user["mesUltimoPlantao"] = 0
            user["anoUltimoPlantao"] = 0
            user["anoHoras"] = 0
            //se spinner for igual a "iOS", então "isIOS" = true
             user["isIOS"] = spinnerOS.text.toString() == "iOS"

            db.collection("usuarios").document(uid).set(user)
                .addOnCompleteListener{
                //Se salvou no database, faz login e manda pra MainActivity
                val email = editTextEmail.text.toString()
                val senha = editTextSenha.text.toString()
                FirebaseAuth.getInstance().signInWithEmailAndPassword(email,senha)
                    .addOnSuccessListener {
                        loadingDialog.hideDialog()
                        val i = Intent(this, MainActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                    //Se falhar o login
                    .addOnFailureListener {
                        loadingDialog.hideDialog()
                        Toast.makeText(this, "Falha ao fazer login: " + it.message, Toast.LENGTH_SHORT)
                            .show()
                    }

            }
            .addOnFailureListener {
                loadingDialog.hideDialog()
                Toast.makeText(this,"Falha ao registrar usuario: " + it.message, Toast.LENGTH_SHORT).show()
            }
    }

}