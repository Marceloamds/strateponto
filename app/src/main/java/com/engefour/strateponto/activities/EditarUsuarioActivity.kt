package com.engefour.strateponto.activities
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.bumptech.glide.Glide
import com.engefour.strateponto.R
import com.engefour.strateponto.uteis.LoadingDialog
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_editar_usuario.*
import java.util.*

class EditarUsuarioActivity : AppCompatActivity() {

    //mudouFoto vai pra true só se a pessoa trocou de foto
    //Dar upload na foto demora e gasta espaço. Impede que a pessoa só clique no "ok"
    private var mudouFoto = false
    private lateinit var loadingDialog: LoadingDialog
    private var uid: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editar_usuario)

        loadingDialog = LoadingDialog(this)
        loadingDialog.showDialog()
        loadingDialog.desativarBotao()

        val db = FirebaseFirestore.getInstance()
        val user = FirebaseAuth.getInstance().currentUser
        uid = FirebaseAuth.getInstance().uid

        //Pega os dados do usuário e popula eles dentro dos editText pra que eles possam ser editados
        db.document("usuarios/$uid").get().addOnSuccessListener {
            val imageUrl = it.get("userFotoUrl").toString()
            val userNome = it.get("userNome").toString()
            val userApelido = it.get("userApelido").toString()
            val email = FirebaseAuth.getInstance().currentUser?.email
            val isIOS = it.getBoolean("isIOS")
            if(!(isDestroyed || isFinishing ))
                Glide.with(this).load(imageUrl).into(imageViewFoto)
            editTextNome.setText(userNome)
            editTextApelido.setText(userApelido)
            editTextEmail.setText(email)
            editTextEmail.isFocusable = false
            editTextEmail.isEnabled = false

            if(isIOS == true)
                spinnerOS.setItems("iOS", "Android")
            else
                spinnerOS.setItems("Android", "iOS")

            loadingDialog.hideDialog()
        }.addOnFailureListener{
            Toast.makeText(this,"Falha ao conectar:" + it.message+ ". Por favor, reenicie o aplicativo.", Toast.LENGTH_LONG)
                .show()
        }

        //Inicializa a galeria do android
        buttonAlterarFoto.setOnClickListener{
            val intent= Intent(Intent.ACTION_PICK)
            intent.type= "image/*"
            startActivityForResult(intent,0)
        }
        backButton.setOnClickListener{
            onBackPressed()
        }

        buttonConfirma.setOnClickListener{
            loadingDialog = LoadingDialog(this)
            loadingDialog.showDialog()
            loadingDialog.desativarBotao()

            if(mudouFoto)
                uploadImageToFirebaseStorage()
            else
                saveUserChanges("")



        }

        buttonTrocarEmail.setOnClickListener{
            //Chama o popup de troca e acessa os elementos
            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCanceledOnTouchOutside(true)
            val view = LayoutInflater.from(this).inflate(R.layout.popup_trocar_email,null)
            val editTextSenha = view.findViewById<EditText>(R.id.editTextSenha)
            val editTextEmail = view.findViewById<EditText>(R.id.editTextEmail)
            val trocarEmailButton = view.findViewById<Button>(R.id.trocarEmailButton)
            trocarEmailButton.setOnClickListener{
                val email = editTextEmail.text.toString()
                val senha = editTextSenha.text.toString()
                if(senha.isEmpty() || email.isEmpty()){
                    Toast.makeText(this, getString(R.string.login_empty), Toast.LENGTH_SHORT).show()
                } else {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Trocar E-mail")
                    builder.setMessage("Deseja mudar o e-mail para  $email?")
                    builder.setPositiveButton("Sim") { _, _ ->
                        loadingDialog.showDialog()
                        //Vê se a senha atual tá correta, se tiver, atualiza o e-mail
                        user?.reauthenticate(
                            EmailAuthProvider.getCredential(user.email!!, senha))
                            ?.addOnSuccessListener {
                                FirebaseAuth.getInstance().currentUser?.updateEmail(email)
                                    ?.addOnSuccessListener {
                                        loadingDialog.hideDialog()
                                        Toast.makeText(this, "E-mail trocado!", Toast.LENGTH_LONG).show()
                                        val i = Intent(this, EditarUsuarioActivity::class.java)
                                        startActivity(i)
                                        finish()
                                    }?.addOnFailureListener {
                                        loadingDialog.hideDialog()
                                        Toast.makeText(this, "Falha ao trocar e-mail: " + it.message, Toast.LENGTH_LONG)
                                            .show()
                                    }
                            }?.addOnFailureListener {
                                loadingDialog.hideDialog()
                                Toast.makeText(this, "Falha ao trocar e-mail: " + it.message, Toast.LENGTH_LONG).show()
                            }
                    }
                    builder.setNegativeButton("Cancelar") { _, _ -> }
                    val alertDialog: AlertDialog = builder.create()
                    alertDialog.show()
                }
            }

            dialog.setContentView(view)
            dialog.show()
        }

        buttonMudarSenha.setOnClickListener{
            //Chama o popup de troca e acessa os elementos
            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCanceledOnTouchOutside(true)
            val view = LayoutInflater.from(this).inflate(R.layout.popup_mudar_senha,null)
            val editTextSenha = view.findViewById<EditText>(R.id.editTextSenha)
            val editTextNovaSenha = view.findViewById<EditText>(R.id.editTextNovaSenha)
            val mudarSenhaButton = view.findViewById<Button>(R.id.mudarSenhaButton)
            mudarSenhaButton.setOnClickListener {
                val senha = editTextSenha.text.toString()
                val novaSenha = editTextNovaSenha.text.toString()
                if(senha.isEmpty() || novaSenha.isEmpty()){
                    Toast.makeText(this, getString(R.string.login_empty), Toast.LENGTH_SHORT).show()
                } else {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Trocar Senha")
                    builder.setMessage("Tem certeza que deseja mudar a senha?")
                    builder.setPositiveButton("Sim") { _, _ ->
                        loadingDialog.showDialog()
                        //Vê se a senha atual tá correta, se tiver, atualiza a senha
                        user?.reauthenticate(EmailAuthProvider.getCredential(user.email!!, senha))
                            ?.addOnSuccessListener {
                                FirebaseAuth.getInstance()
                                    .currentUser?.updatePassword(novaSenha)
                                    ?.addOnSuccessListener {
                                        loadingDialog.hideDialog()
                                        Toast.makeText(this, "E-mail trocado!", Toast.LENGTH_LONG).show()
                                        val i = Intent(this, EditarUsuarioActivity::class.java)
                                        startActivity(i)
                                        finish()
                                    }?.addOnFailureListener {
                                        loadingDialog.hideDialog()
                                        Toast.makeText(this, "Falha ao mudar senha: " + it.message, Toast.LENGTH_LONG)
                                            .show()
                                    }
                            }?.addOnFailureListener {
                                loadingDialog.hideDialog()
                                Toast.makeText(this, "Falha ao mudar senha: " + it.message, Toast.LENGTH_LONG).show()
                            }
                    }
                    builder.setNegativeButton("Cancelar") { _, _ -> }
                    val alertDialog: AlertDialog = builder.create()
                    alertDialog.show()
                }
            }
            dialog.setContentView(view)
            dialog.show()
        }
    }

    private var selectedPhotoUri: Uri? = null

    //Quando o usuário seleciona a imagem
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data!= null){
            selectedPhotoUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver,selectedPhotoUri)
            imageViewFoto.setImageBitmap(bitmap)
            mudouFoto = true
        }
    }

    private fun saveUserChanges(userFotoUrl:String){
        loadingDialog.changeText("Salvando mudanças")
        val usuario = HashMap<String, Any>()
        if(userFotoUrl != "")
            usuario["userFotoUrl"] = userFotoUrl
        usuario["userNome"] = editTextNome.text.toString()
        usuario["userApelido"] = editTextApelido.text.toString()
        usuario["isIOS"] = (spinnerOS.text.toString() == "iOS")
        FirebaseFirestore.getInstance().document("usuarios/$uid")
            .update(usuario).addOnSuccessListener {
                loadingDialog.hideDialog()
                Toast.makeText(this,"Usuario atualizado!", Toast.LENGTH_SHORT).show()
                val i = Intent(this, UsuarioActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                i.putExtra("userId",uid)
                startActivity(i)
            }
            .addOnFailureListener{
                loadingDialog.hideDialog()
                Toast.makeText(this,"Falha ao atualizar usuario: " + it.message, Toast.LENGTH_SHORT).show()
            }
    }

    // Manda a imagem pro Firebase Storage
     private fun  uploadImageToFirebaseStorage() {
        loadingDialog.changeText("Upando a imagem")
        val filename = UUID.randomUUID().toString()
        val ref =  FirebaseStorage.getInstance().getReference("image/$filename")
        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener { uri->
                    saveUserChanges(uri.toString())
                    //  uri.toString() = link para download
                }
            .addOnFailureListener{
                loadingDialog.hideDialog()
                Toast.makeText(this,"Falha ao registrar foto: " + it.message, Toast.LENGTH_SHORT).show()
            }
        }

    }
}
