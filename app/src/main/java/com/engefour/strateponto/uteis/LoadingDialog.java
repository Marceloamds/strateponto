package com.engefour.strateponto.uteis;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.engefour.strateponto.R;

//Usado pra colocar um gif com a biblicoteca "Glide" dentro de uma caixa de dialogo
//Copiei a maioria do código online.
public class LoadingDialog {

    private Activity activity;
    private Dialog dialog;
    //..we need the context else we can not create the dialog so get context in constructor
    public LoadingDialog(Activity activity) {
        this.activity = activity;
    }

    public void showDialog() {

        dialog  = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //...set cancelable false so that it's never get hidden
        dialog.setCancelable(false);
        //...that's the layout i told you will inflate later
        dialog.setContentView(R.layout.custom_loading_layout);


        //...initialize the imageView form infalted layout
        ImageView gifImageView = dialog.findViewById(R.id.custom_loading_imageView);

        /*
        it was never easy to load gif into an ImageView before Glide or Others library
        and for doing this we need DrawableImageViewTarget to that ImageView
        */

        DrawableImageViewTarget imageViewTarget = new DrawableImageViewTarget(gifImageView);

        //...now load that gif which we put inside the drawble folder here with the help of Glide

        Glide.with(activity)
                .load(R.drawable.loading)
                .centerCrop()
                .into(imageViewTarget);
        Button buttonRefresh = dialog.findViewById(R.id.buttonRefresh);
        buttonRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity,activity.getClass());
                i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                activity.finish();
                activity.startActivity(i);
            }
        });

        //...finaly show it
        dialog.show();
    }

    //..also create a method which will hide the dialog when some work is done
    public void hideDialog(){
        if (dialog != null && dialog.isShowing() && (dialog.getWindow()!=null) && !activity.isFinishing()) {
            dialog.dismiss();
        }
    }

    public void changeText(String text){
       TextView textViewLoadingText = dialog.findViewById(R.id.textViewLoading);
       textViewLoadingText.setText(text+"...");
    }

    public  void  desativarBotao(){
        Button buttonRefresh = dialog.findViewById(R.id.buttonRefresh);
        buttonRefresh.setVisibility(View.GONE);
        buttonRefresh.setClickable(false);
        buttonRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity,"Não é possível recarregar neste momento",Toast.LENGTH_LONG).show();
            }
        });

    }

    public void resetText(){
        TextView textViewLoadingText = dialog.findViewById(R.id.textViewLoading);
        textViewLoadingText.setText("Aguarde...");
    }

}