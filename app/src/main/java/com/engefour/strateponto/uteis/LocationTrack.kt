package com.engefour.strateponto.uteis

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import java.util.*
import android.graphics.BitmapFactory
import androidx.core.content.ContextCompat
import com.engefour.strateponto.R
import com.engefour.strateponto.activities.MainActivity


class LocationTrack : Service(), LocationListener {

    internal var isGPSEnable = false
    internal var isNetworkEnable = false
    internal var latitude: Double = 0.toDouble()
    internal var longitude: Double = 0.toDouble()
    internal var locationManager: LocationManager? = null
    internal var location: Location? = null
    private val mHandler = Handler()
    private var mTimer: Timer? = null
    internal var notify_interval: Long = 10000
    internal var intent: Intent? = null
    internal var uid:String? = null
    private lateinit var builder: Notification.Builder

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        mTimer = Timer()
        mTimer!!.schedule(TimerTaskToGetLocation(), 10, notify_interval)
        intent = Intent(str_receiver)
        criarNotificacao()
        fn_getlocation()

    }

    override fun onLocationChanged(location: Location) {
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {

    }

    override fun onProviderEnabled(provider: String) {

    }

    override fun onProviderDisabled(provider: String) {

    }

    @SuppressLint("MissingPermission")
    private fun fn_getlocation() {
        locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        isGPSEnable = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        isNetworkEnable = locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        if (isNetworkEnable) {
            location = null
            locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 30, 0f, this)
            if (locationManager != null) {
                location = locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                if (location != null) {

                    val locationA = Location("point A")

                    locationA.latitude = -20.5031089
                    locationA.longitude = -54.6109947

                    val locationB = Location("point B")

                    locationB.latitude = location!!.latitude
                    locationB.longitude = location!!.longitude

                    val distance = locationA.distanceTo(locationB)

                    Log.d("distance", (distance - location!!.accuracy).toString())
                    if((distance - location!!.accuracy) > 100)
                        logOut()


                    latitude = location!!.latitude
                    longitude = location!!.longitude
                    fn_update(location!!)
                }
            }
        } else if (isGPSEnable){
            location = null
            locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 30 , 0f, this)
            if (locationManager != null) {
                location = locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                if (location != null) {
                    val locationA = Location("point A")

                    locationA.latitude = -20.50298311
                    locationA.longitude = -54.6109947

                    val locationB = Location("point B")

                    locationB.latitude = location!!.latitude
                    locationB.longitude = location!!.longitude

                    val distance = locationA.distanceTo(locationB)
                    Log.d("distance", (distance - location!!.accuracy).toString())
                    if((distance - location!!.accuracy) > 100)
                        logOut()


                    latitude = location!!.latitude
                    longitude = location!!.longitude
                    fn_update(location!!)
                }
            }
        }
    }

    private inner class TimerTaskToGetLocation : TimerTask() {
        override fun run() {

            mHandler.post { fn_getlocation() }

        }
    }

    // Faz o logout de um usuário
    // Código tá ruim. Talvez reaproveitar o login/logout do PontoFragment de algum jeito? Copiei o código de lá
    // Pega o horário do id que tá aberto e fecha. Não tem como fugir, mas pode usar menos código
    private fun logOut(){
        uid = FirebaseAuth.getInstance().uid

        val db = FirebaseFirestore.getInstance()
        val userRef =  db.document("usuarios/$uid")
        userRef.get().addOnSuccessListener {
            if(it.getBoolean("isOnline") == true) {
                // Pega o horário de entrada e compara com o de saída pra fazer o cálculo de segundos
                val horarioEntrada = it.get("horaEntrada").toString()
                val dateEntrada = DateTime.parse(
                    horarioEntrada,
                    DateTimeFormat.forPattern("dd-MM-yy hh.mm.ss aa").withLocale(Locale("pt", "BR"))
                )
                val p = Period(dateEntrada, DateTime.now(DateTimeZone.getDefault()))
                var segundosTotais = p.toStandardSeconds().seconds
                if (segundosTotais < 60) {
                    userRef.update("isOnline", false)
                } else {
                    // 12 horas de trabalho é o máximo por um plantão. Impede que alguém esqueça de bater ponto na saída
                    if (segundosTotais > 43200)
                        segundosTotais = 43200
                    val usuarioHorario = HashMap<String, Any>()

                    //Se o ultimo plantão foi no ano passado
                    if (DateTime.now(DateTimeZone.getDefault()).year != it.get("anoUltimoPlantao").toString().toInt()) {
                        //Esse ano tem o horário total desse plantão
                        usuarioHorario["anoHoras"] = segundosTotais
                        usuarioHorario["anoUltimoPlantao"] = DateTime.now(DateTimeZone.getDefault()).year

                        // Se o ultimo plantão nesse ano, só soma ao total
                    } else
                        usuarioHorario["anoHoras"] = it.get("anoHoras").toString().toInt() + segundosTotais

                    //Se o último plantão foi mês passado
                    if (DateTime.now(DateTimeZone.getDefault()).monthOfYear != it.get("mesUltimoPlantao").toString().toInt()) {

                        //Vê se faz mais de um mês desde o ultimo plantão
                        if (it.get("mesUltimoPlantao").toString().toInt() < (DateTime.now(DateTimeZone.getDefault()).monthOfYear - 1))
                            usuarioHorario["mesPassadoHoras"] = 0
                        else
                            usuarioHorario["mesPassadoHoras"] = it.get("mesHoras").toString().toInt()

                        //Esse mês tem o horário total desse plantão
                        usuarioHorario["mesHoras"] = segundosTotais
                        usuarioHorario["mesUltimoPlantao"] = DateTime.now(DateTimeZone.getDefault()).monthOfYear

                        // Se o ultimo plantão nesse mês, só soma ao total
                    } else
                        usuarioHorario["mesHoras"] = it.get("mesHoras").toString().toInt() + segundosTotais


                    //Se o ultimo plantão foi semana passada
                    if (DateTime.now(DateTimeZone.getDefault()).weekOfWeekyear != it.get("semanaUltimoPlantao").toString().toInt()) {
                        //Vê se faz mais de uma semana desde o ultimo plantão
                        if (it.get("semanaUltimoPlantao").toString().toInt() < (DateTime.now(DateTimeZone.getDefault()).weekOfWeekyear - 1))
                            usuarioHorario["semanaPassadaHoras"] = 0
                        else
                            usuarioHorario["semanaPassadaHoras"] = it.get("semanaHoras").toString().toInt()

                        //Essa semana tem o horário total desse plantão
                        usuarioHorario["semanaHoras"] = segundosTotais
                        usuarioHorario["semanaUltimoPlantao"] = DateTime.now(DateTimeZone.getDefault()).weekOfWeekyear

                        //Se o ultimo plantão foi nessa semana, só soma ao total
                    } else
                        usuarioHorario["semanaHoras"] = it.get("semanaHoras").toString().toInt() + segundosTotais

                    usuarioHorario["isOnline"] = false

                    userRef.update(usuarioHorario)
                }
            }
        }
    }

    // O android cria notificações de jeitos diferentes de API < 26 e > 26
    @Suppress("DEPRECATION")
    private fun criarNotificacao() {
        val channelId = "com.engefour.engeponto"
        val i = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT)

        //Se API > 26
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, "StratePonto", NotificationManager.IMPORTANCE_HIGH)
            channel.enableLights(true)
            channel.lightColor = ContextCompat.getColor(this, R.color.light)
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(channel)

            builder = Notification.Builder(this, channelId)
                .setContentTitle(getString(R.string.titulo_notificacao))
                .setContentText(getString(R.string.texto_notificacao))
                .setSmallIcon(R.drawable.ic_relogio)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.logo))
                .setOngoing(true)
                .addAction(R.drawable.ic_edit, "Sair do plantão", pendingIntent)
                .setContentIntent(pendingIntent)
            startForeground(1234,builder.build())

        } else {
            //Se API < 26
            builder = Notification.Builder(this)
                .setContentTitle(getString(R.string.titulo_notificacao))
                .setContentText(getString(R.string.texto_notificacao))
                .setSmallIcon(R.drawable.ic_relogio)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.logo))
                .setOngoing(true)
                .addAction(R.drawable.ic_edit, "Sair do plantão", pendingIntent)
                .setContentIntent(pendingIntent)
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(1234, builder.build())
        }
    }

    private fun fn_update(location: Location) {

        intent!!.putExtra("latitude", location.latitude.toString() + "")
        intent!!.putExtra("longitude", location.longitude.toString() + "")
        sendBroadcast(intent)
    }

    companion object {
        var str_receiver = "servicetutorial.service.receiver"
    }

}