package com.engefour.strateponto.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.crashlytics.android.Crashlytics
import com.engefour.strateponto.uteis.LoadingDialog
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.row_feed.view.*
import kotlinx.android.synthetic.main.fragment_feed.view.*
import android.app.DownloadManager
import android.view.*
import com.engefour.strateponto.activities.NovaNoticiaActivity
import com.engefour.strateponto.R
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat
import java.util.*


class FeedFragment : Fragment() {

    private var listener: OnFragmentInteractionListener? = null
    lateinit var loadingDialog: LoadingDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loadingDialog = LoadingDialog(activity)
        loadingDialog.showDialog()
        val view =  inflater.inflate(R.layout.fragment_feed, container, false)
        setHasOptionsMenu(true)

        val adapter = GroupAdapter<ViewHolder>()
        view.listviewFeed.adapter = adapter
        val db = FirebaseFirestore.getInstance()
        db.collection("feed").limit(10).orderBy("noticiaHora", Query.Direction.DESCENDING)
            .addSnapshotListener { noticia, error ->
            if (error != null) {
                Crashlytics.logException(error)
            } else if (noticia != null && context != null) {
                adapter.clear()
                noticia.forEach {
                    adapter.add(
                        UserItem(
                            context!!,
                            it.get("noticiaAnexo").toString(),
                            it.get("noticiaTitulo").toString(),
                            it.get("noticiaDescricao").toString(),
                            it.get("userFotoUrl").toString(),
                            it.get("userApelido").toString(),
                            it.get("nomeArquivo").toString(),
                            it.get("noticiaHora").toString()
                        )
                    )
                }
            }
                loadingDialog.hideDialog()
        }

        view.novaNoticiaButton.setOnClickListener{
            val i = Intent(activity, NovaNoticiaActivity::class.java)
            startActivity(i)
        }

        val layoutManager = LinearLayoutManager(activity)
        view.listviewFeed.layoutManager = layoutManager

        return view
    }

    class UserItem(private val context: Context, private val pdfUrl:String, private val feedTitulo:String,
                   private val feedDescricao:String, private val userFotoUrl:String,
                   private val userApelido:String, private val nomeArquivo:String,
                   private val noticiaHora:String): Item<ViewHolder>(){

        override fun bind(viewHolder: ViewHolder, position: Int) {
            viewHolder.itemView.textViewTitulo.text = feedTitulo
            viewHolder.itemView.textViewDescricao.text = feedDescricao
            viewHolder.itemView.userNome.text = userApelido
            Glide.with(viewHolder.itemView).load(userFotoUrl).into(viewHolder.itemView.userFoto)

            if (pdfUrl == ""){
                viewHolder.itemView.downloadButton.visibility = View.GONE
                viewHolder.itemView.downloadButton.isClickable = false
            }else{
                viewHolder.itemView.downloadButton.visibility = View.VISIBLE
                viewHolder.itemView.downloadButton.isClickable = true
            }

            val noticiaDate = DateTime.parse(noticiaHora,
                DateTimeFormat.forPattern("yy-MM-dd HH.mm.ss")
                    .withLocale(Locale("pt", "BR")))

            val sdf = SimpleDateFormat("MMMM", Locale("pt", "BR"))
            val monthOfYear = sdf.format(noticiaDate.toDate()).capitalize()

            val noticiaHoraString = String.format(
                "%02d de $monthOfYear as %02dh%02d",
                noticiaDate.dayOfMonth,
                noticiaDate.hourOfDay,
                noticiaDate.minuteOfHour)
            viewHolder.itemView.horaNoticia.text = noticiaHoraString

            viewHolder.itemView.downloadButton.setOnClickListener{
                downloadFile(context,nomeArquivo,".pdf","Downloads",pdfUrl)
            }
        }
        override fun getLayout(): Int {
            return R.layout.row_feed
        }

        private fun downloadFile(context: Context, fileName: String, fileExtension: String, destinationDirectory: String, url: String): Long {


            val downloadmanager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            val uri = Uri.parse(url)
            val request = DownloadManager.Request(uri)

            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            request.setDestinationInExternalFilesDir(context, destinationDirectory, fileName + fileExtension)

            return downloadmanager.enqueue(request)
        }

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(): FeedFragment {
            return FeedFragment()
        }
    }
}
