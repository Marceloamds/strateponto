package com.engefour.strateponto.fragments
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.crashlytics.android.Crashlytics
import com.engefour.strateponto.activities.MainActivity
import com.engefour.strateponto.R
import com.engefour.strateponto.activities.UsuarioActivity
import com.engefour.strateponto.uteis.LoadingDialog
import com.engefour.strateponto.uteis.LocationTrack
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_ponto.view.*
import kotlinx.android.synthetic.main.row_online.view.*
import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat
import java.util.*

//É a classe mais complexa do app. Código tá um pouco bagunçado pq tem muita coisa
class   PontoFragment : androidx.fragment.app.Fragment() {

    private var notificationManager:NotificationManager? = null
    private var listener: OnFragmentInteractionListener? = null
    private var isOnline = false
    private lateinit var loadingDialog: LoadingDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {



        val view: View = inflater.inflate(R.layout.fragment_ponto, container, false)
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val db = FirebaseFirestore.getInstance()
        notificationManager = activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        loadingDialog = LoadingDialog(activity)
        loadingDialog.showDialog()

        //Popula o recycler view de pessoas online
        val adapter = GroupAdapter<ViewHolder>()
        view.recyclerViewOnline.adapter = adapter

        if(uid!="")
            db.collection("usuarios").whereEqualTo("isOnline",true)
                .whereEqualTo("isActive",true).addSnapshotListener { documentSnapshot, error ->
                    if (error != null) {
                        Crashlytics.logException(error)
                    } else if (documentSnapshot != null && context!=null) {
                        adapter.clear()
                        when (documentSnapshot.size()){
                            0 -> view.textViewOnline.text = activity?.getString(R.string.nenhum_usuario)
                            1 -> view.textViewOnline.text = activity?.getString(R.string.usuario_sozinho, documentSnapshot.documents[0].get("userApelido").toString())
                            in 2..12 -> view.textViewOnline.text = activity?.getString(R.string.usuarios_online,documentSnapshot.size())
                            else -> view.textViewOnline.text = activity?.getString(R.string.muitos_usuarios)
                        }
                        documentSnapshot.forEach {
                            adapter.add(
                                UserItem(
                                    context,
                                    it.get("userFotoUrl").toString(),
                                    it.get("userApelido").toString(),
                                    it.get("userId").toString()
                                )
                            )
                        }
                    }
                }

        // popula o TextView do café
        if(uid!="")
            db.document("sala/cafe")
                .addSnapshotListener { documentSnapshot, error ->
                    if (error != null) {
                        Crashlytics.logException(error)
                    } else if (documentSnapshot != null && context!=null) {
                        //Atualiza o café na tela inicial. Fiz desse jeito pra ser compatível com a versão anterior.
                        val horaCafe = documentSnapshot.get("horaCafe").toString()

                        val dateCafe =
                            DateTime.parse(horaCafe, DateTimeFormat.forPattern("dd-MM-yy hh.mm.ss aa").withLocale(Locale("pt","BR")))
                        val hora =  String.format("%02dh%02d",dateCafe.hourOfDay,dateCafe.minuteOfHour)
                        when {
                            dateCafe.dayOfYear == DateTime.now().dayOfYear ->
                                view.textViewCafe.text = activity?.getString(R.string.cafe_hoje,hora)
                            dateCafe.dayOfYear == DateTime.now().dayOfYear -1 ->
                                view.textViewCafe.text = activity?.getString(R.string.cafe_ontem,hora)
                            else -> {
                                val sdf = SimpleDateFormat("EEEE",Locale("pt","BR"))
                                val dayOfTheWeek = sdf.format(dateCafe.toDate())
                                view.textViewCafe.text = activity?.getString(R.string.cafe_tempo,hora,dayOfTheWeek)
                            }
                        }
                    }
                }

        if(uid!= "" && context!= null)
            view.constraintLayoutEstado.setOnClickListener {
                //Chama o popup de troca e acessa os elementos
                val dialog = Dialog(context!!)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.setCanceledOnTouchOutside(true)
                dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
                val viewPopup = LayoutInflater.from(context!!).inflate(R.layout.popup_escolher_estado, null)
                val estadoNormal = viewPopup.findViewById<ConstraintLayout>(R.id.constraintLayoutNormal)
                val estadoReuniao = viewPopup.findViewById<ConstraintLayout>(R.id.constraintLayoutDiretoria)
                val estadoCliente = viewPopup.findViewById<ConstraintLayout>(R.id.constraintLayoutCliente)

                estadoNormal.setOnClickListener{
                    criarDialogoSala(activity?.resources?.getString(R.string.normal_sala_normal))
                    dialog.cancel()

                }
                estadoReuniao.setOnClickListener{
                    criarDialogoSala(activity?.resources?.getString(R.string.normal_sala_diretoria))
                    dialog.cancel()
                }
                estadoCliente.setOnClickListener{
                    criarDialogoSala(activity?.resources?.getString(R.string.normal_sala_cliente))
                    dialog.cancel()
                }

                dialog.setContentView(viewPopup)
                dialog.show()
            }

        // Popula o TextView do estado de sala
        if(uid!="")
            db.document("sala/estadoSala")
                .addSnapshotListener { documentSnapshot, error ->
                    if (error != null) {
                        Crashlytics.logException(error)
                    } else if (documentSnapshot != null && context!=null) {
                        when(documentSnapshot.get("estadoSala")){
                            "Sala normal" ->{
                                view.textViewEstadoSala.text = activity?.resources?.getString(R.string.subnormal_sala_normal)
                                view.imageViewEstadoSala.setImageDrawable(context?.let { ContextCompat.getDrawable(it,
                                    R.drawable.ic_working
                                ) })
                            }
                            "Reunião de diretoria/gerência"->{
                                view.textViewEstadoSala.text = activity?.resources?.getString(R.string.subnormal_sala_diretoria)
                                view.imageViewEstadoSala.setImageDrawable(context?.let { ContextCompat.getDrawable(it,
                                    R.drawable.ic_diretoria
                                ) })
                            }
                            "Reunião com cliente" ->{
                                view.textViewEstadoSala.text = activity?.resources?.getString(R.string.subnormal_sala_cliente)
                                view.imageViewEstadoSala.setImageDrawable(context?.let { ContextCompat.getDrawable(it,
                                    R.drawable.ic_cliente
                                ) })
                            }
                            else ->{
                                view.textViewEstadoSala.text = activity?.resources?.getString(R.string.subnormal_sala_normal)
                                view.imageViewEstadoSala.setImageDrawable(context?.let { ContextCompat.getDrawable(it,
                                    R.drawable.ic_working
                                ) })
                            }
                        }
                    }
                }


        //Se o usuário estiver online, busca o horário no banco e coloca no textview
        if(uid!="")
            db.document("usuarios/$uid").get().addOnSuccessListener {
                if(activity!= null)
                    if(!(activity!!.isDestroyed || activity!!.isFinishing))
                        Glide.with(this).load(it.getString("userFotoUrl")).into(view.userImageView)

                view.userImageView.setOnClickListener { _ ->
                    val i = Intent(activity, UsuarioActivity::class.java)
                    i.putExtra("userId",it.getString("userId"))
                    startActivity(i)
                }

                if(it.getBoolean("isOnline")!= null) {
                    isOnline = it.getBoolean("isOnline")!!
                    if (isOnline) {
                        val horaEntrada = DateTime.parse(
                            it.get("horaEntrada").toString(),
                            DateTimeFormat.forPattern("dd-MM-yy hh.mm.ss aa").withLocale(Locale("pt","BR")))
                        view.textViewPonto.textSize = 20F
                        val hora = String.format("%02dh%02d",horaEntrada.hourOfDay,horaEntrada.minuteOfHour)
                        view.textViewPonto.text = activity?.getString(R.string.plantao_iniciado,hora)
                        view.buttonPonto.text = activity?.getString(R.string.button_finalizar_plantao)
//                        locationTrack()
                    }else {
//                        stopService()
                        if (notificationManager != null)
                            notificationManager?.cancelAll()
                    }

                    loadingDialog.hideDialog()
                }
                //se o "isOnline" é nulo, teve erro de conexão
                else {
                    Toast.makeText(activity,"Falha ao conectar. Por favor, reenicie o aplicativo.", Toast.LENGTH_LONG)
                        .show()
                }
            }.addOnFailureListener{
                Toast.makeText(activity,"Falha ao conectar:" + it.message+ ". Por favor, reenicie o aplicativo.", Toast.LENGTH_LONG)
                    .show()
            }

        if(uid!="")
            view.constraintLayoutCafe.setOnClickListener{
                val builder = AlertDialog.Builder(activity)
                val hora = String.format("%02dh%02d de hoje",DateTime.now().hourOfDay,DateTime.now().minuteOfHour)
                builder.setTitle("Obrigado pelo café! <3")
                builder.setMessage("Deseja mudar o horário do último café para $hora?")
                builder.setPositiveButton("Sim!") { _, _ ->
                    val cafe = HashMap<String, Any>()
                    val horaCompleta = DateTime.now().toString("dd-MM-yy hh.mm.ss aa",Locale("pt", "BR"))
                    cafe["horaCafe"] = horaCompleta
                    FirebaseFirestore.getInstance().document("sala/cafe")
                        .update(cafe)
                }
                builder.setNegativeButton("Cancelar") { _, _ -> }
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }


        view.buttonPonto.setOnClickListener{
            if(!isOnline){
                view.buttonPonto.startAnimation()
                logIn(uid)
            }else{
                view.buttonPonto.startAnimation()
                logOut(uid)
            }
        }
        return view
    }

    private fun locationTrack(){
        if(activity!=null)
            if (ContextCompat.checkSelfPermission(
                    activity!!.applicationContext,
                    android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                val serviceIntent =  Intent(activity, LocationTrack::class.java)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    activity!!.startForegroundService(serviceIntent)
                } else {
                    activity!!.startService(serviceIntent)
                }
            }
    }

    private fun stopService(){
        if(activity!=null){
            val serviceIntent =  Intent(activity, LocationTrack::class.java)
            activity!!.stopService(serviceIntent)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) listener = context else {
            throw RuntimeException("$context must implement  OnFragmentInteractionListener")
        }
    }



    //Faz entrada no horário do plantão
    private fun logIn(uid: String){
        val usuarioHorario = HashMap<String, Any>()
        val hora =
            String.format("%02dh%02d",DateTime.now().hourOfDay,DateTime.now().minuteOfHour)
        val horaCompleta = DateTime.now().toString("dd-MM-yy hh.mm.ss aa",Locale("pt", "BR"))
        usuarioHorario["horaEntrada"] = horaCompleta
        usuarioHorario["isOnline"] = true
        val db = FirebaseFirestore.getInstance()
        db.document("usuarios/$uid").update(usuarioHorario)
            .addOnSuccessListener {
                if(context != null) {
                    isOnline = true
                    view?.textViewPonto?.textSize = 20F
                    view?.textViewPonto?.text = activity?.getString(R.string.plantao_iniciado, hora)
                    //Tentei deixar um icone bonitinho no final
                    val icon = BitmapFactory.decodeResource(resources, R.drawable.logo)
                    view?.buttonPonto?.doneLoadingAnimation(0xD3D7DA, icon)
                    view?.buttonPonto?.revertAnimation {
                        view?.buttonPonto?.setBackgroundColor(
                            ContextCompat.getColor(
                                context!!,
                                R.color.colorPrimaryDark
                            )
                        )
                        view?.buttonPonto?.text = activity?.getString(R.string.button_finalizar_plantao)
                        view?.buttonPonto?.background =
                            ContextCompat.getDrawable(context!!,
                                R.drawable.rounded_button_orange
                            )
                    }
                }
                //Se a pessoa tá na sala, ela não recebe notificações do estado e do café pelo Firebase Messaging
                FirebaseMessaging.getInstance().unsubscribeFromTopic("offline")
//                locationTrack()
            }
            .addOnFailureListener{
                loadingDialog.hideDialog()
                Toast.makeText(activity,"Falha ao registrar plantão: " + it.message, Toast.LENGTH_SHORT)
                    .show()
            }

    }

    // Faz saída no horário do plantão e calcula horas totais
    private fun logOut(uid:String){
        val db = FirebaseFirestore.getInstance()
        val userRef =  db.document("usuarios/$uid")
        userRef.get().addOnSuccessListener {
            val isReallyOnline = it.getBoolean("isOnline")
            if (isReallyOnline == false) {
                Toast.makeText(activity, "Plantão já marcado", Toast.LENGTH_SHORT).show()
                val i = Intent(activity, MainActivity::class.java)
                startActivity(i)
                activity?.finish()
            } else {
                // Pega o horário de entrada e compara com o de saída pra fazer o cálculo de segundos
                val horarioEntrada = it.get("horaEntrada").toString()

                val dateEntrada = DateTime.parse(
                    horarioEntrada,
                    DateTimeFormat.forPattern("dd-MM-yy hh.mm.ss aa").withLocale(Locale("pt", "BR"))
                )
                val p = Period(dateEntrada, DateTime.now())
                var segundosTotais = p.toStandardSeconds().seconds
                if (segundosTotais < 60) {
                    isOnline = false
                    userRef.update("isOnline", false)
                    FirebaseMessaging.getInstance().subscribeToTopic("offline")
                    view?.textViewPonto?.textSize = 20F
                    view?.textViewPonto?.text = activity?.getString(R.string.plantao_nao_contabilizado)
                    view?.buttonPonto?.revertAnimation {
                        view?.buttonPonto?.text = activity?.getString(R.string.button_comecar_plantao)
                    }
                } else {
                    // 12 horas de trabalho é o máximo por um plantão. Impede que alguém esqueça de bater ponto na saída
                    if (segundosTotais > 43200)
                        segundosTotais = 43200

                    val usuarioHorario = HashMap<String, Any>()
                    val hora =
                        String.format("%02dh%02d", DateTime.now().hourOfDay, DateTime.now().minuteOfHour)

                    //Se o ultimo plantão foi no ano passado
                    if (DateTime.now().year != it.get("anoUltimoPlantao").toString().toInt()) {
                        //Esse ano tem o horário total desse plantão
                        usuarioHorario["anoHoras"] = segundosTotais
                        usuarioHorario["anoUltimoPlantao"] = DateTime.now().year

                        // Se o ultimo plantão nesse ano, só soma ao total
                    } else
                        usuarioHorario["anoHoras"] = it.get("anoHoras").toString().toInt() + segundosTotais

                    //Se o último plantão foi mês passado
                    if (DateTime.now().monthOfYear != it.get("mesUltimoPlantao").toString().toInt()) {

                        //Vê se faz mais de um mês desde o ultimo plantão
                        if (it.get("mesUltimoPlantao").toString().toInt() < (DateTime.now().monthOfYear - 1))
                            usuarioHorario["mesPassadoHoras"] = 0
                        else
                            usuarioHorario["mesPassadoHoras"] = it.get("mesHoras").toString().toInt()

                        //Esse mês tem o horário total desse plantão
                        usuarioHorario["mesHoras"] = segundosTotais
                        usuarioHorario["mesUltimoPlantao"] = DateTime.now().monthOfYear

                        // Se o ultimo plantão nesse mês, só soma ao total
                    } else
                        usuarioHorario["mesHoras"] = it.get("mesHoras").toString().toInt() + segundosTotais


                    //Se o ultimo plantão foi semana passada
                    if (DateTime.now().weekOfWeekyear != it.get("semanaUltimoPlantao").toString().toInt()) {
                        //Vê se faz mais de uma semana desde o ultimo plantão
                        if (it.get("semanaUltimoPlantao").toString().toInt() < (DateTime.now().weekOfWeekyear - 1))
                            usuarioHorario["semanaPassadaHoras"] = 0
                        else
                            usuarioHorario["semanaPassadaHoras"] = it.get("semanaHoras").toString().toInt()

                        //Essa semana tem o horário total desse plantão
                        usuarioHorario["semanaHoras"] = segundosTotais
                        usuarioHorario["semanaUltimoPlantao"] = DateTime.now().weekOfWeekyear

                        //Se o ultimo plantão foi nessa semana, só soma ao total
                    } else
                        usuarioHorario["semanaHoras"] = it.get("semanaHoras").toString().toInt() + segundosTotais

                    usuarioHorario["isOnline"] = false
                    userRef.update(usuarioHorario)
                        .addOnSuccessListener {
                            isOnline = false
                            val horas: Int = segundosTotais / 3600
                            val minutos: Int = (segundosTotais - horas * 3600) / 60
                            val segundos = segundosTotais % 60
                            val tempoTotal = String.format("%02dh%02dmin%02ds", horas, minutos, segundos)

                            view?.textViewPonto?.textSize = 20F
                            view?.textViewPonto?.text =
                                activity?.getString(R.string.plantao_finalizado, hora, tempoTotal)
                            view?.buttonPonto?.revertAnimation {
                                view?.buttonPonto?.text = activity?.getString(R.string.button_comecar_plantao)
                                view?.buttonPonto?.setBackgroundColor(
                                    ContextCompat.getColor(
                                        context!!,
                                        R.color.colorPrimaryDark
                                    )
                                )
                                view?.buttonPonto?.background =
                                    ContextCompat.getDrawable(context!!,
                                        R.drawable.rounded_button_orange
                                    )
                            }
                            //Se a pessoa não tá na sala, ela recebe notificações do estado e do café pelo Firebase Messaging
                            FirebaseMessaging.getInstance().subscribeToTopic("offline")
//                            stopService()
                        }
                        .addOnFailureListener { error ->
                            loadingDialog.hideDialog()
                            Toast.makeText(activity, "Falha ao registrar plantão: " + error.message, Toast.LENGTH_SHORT)
                                .show()
                        }
                }
                //tira a notificação do plantão
                notificationManager!!.cancel(1234)
            }
        }   .addOnFailureListener { error ->
            loadingDialog.hideDialog()
            Toast.makeText(activity, "Falha ao registrar plantão: " + error.message, Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun criarDialogoSala(estadoSala:String?){
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Mudar estado")
        builder.setMessage("Deseja mudar o estado da sala para $estadoSala?")
        builder.setPositiveButton("Sim") { _, _ ->
            val sala = HashMap<String, Any>()
            if(estadoSala != null)
                sala["estadoSala"] = estadoSala
            FirebaseFirestore.getInstance().document("sala/estadoSala")
                .update(sala).addOnSuccessListener {
                    Toast.makeText(activity,"Estado de sala alterado!", Toast.LENGTH_LONG).show()
                }
        }
        builder.setNegativeButton("Cancelar") { _, _ -> }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }
    companion object {
        @JvmStatic
        fun newInstance(): PontoFragment {
            return PontoFragment()
        }
    }

    //Classe do Groupie que infla a view para o listview
    class UserItem(private val context: Context?,private val imageUrl:String, private val userApelido:String,private val userId:String): Item<ViewHolder>(){
        override fun bind(viewHolder: ViewHolder, position: Int) {
            viewHolder.itemView.textViewOnlineRow.text = userApelido
            Glide.with(viewHolder.itemView).load(imageUrl).into(viewHolder.itemView.imageViewOnlineRow)
            viewHolder.itemView.imageViewOnlineRow.setOnClickListener {
                val i = Intent(context, UsuarioActivity::class.java)
                i.putExtra("userId", userId)
                context?.startActivity(i)
            }
        }
        override fun getLayout(): Int {
            return R.layout.row_online
        }
    }
}