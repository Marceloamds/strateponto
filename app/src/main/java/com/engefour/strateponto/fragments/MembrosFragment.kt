package com.engefour.strateponto.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.engefour.strateponto.R
import com.engefour.strateponto.uteis.LoadingDialog
import com.engefour.strateponto.viewpager.ViewPagerMembrosAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_membros.view.*


class MembrosFragment : androidx.fragment.app.Fragment() {

    private var listener: OnFragmentInteractionListener? = null
    lateinit var loadingDialog: LoadingDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loadingDialog = LoadingDialog(activity)
        loadingDialog.showDialog()
        val view =  inflater.inflate(R.layout.fragment_membros, container, false)
        val fragmentAdapter = ViewPagerMembrosAdapter(childFragmentManager)
        view.viewPagerMembros.isSaveFromParentEnabled = true
        view.viewPagerMembros.isSaveEnabled = true
        view.viewPagerMembros.adapter = fragmentAdapter

        //Sincroniza tabsMembros com o viewMembros
        view.tabsMembros.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(view.viewPagerMembros))
        view.viewPagerMembros.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(view.tabsMembros))

        return view
    }

    interface AtualizarListaMembros{
        fun atualizarListaMembros()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(): MembrosFragment {
            return MembrosFragment()
        }
    }
}