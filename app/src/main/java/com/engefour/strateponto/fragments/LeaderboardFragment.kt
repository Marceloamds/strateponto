package com.engefour.strateponto.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.engefour.strateponto.R
import com.engefour.strateponto.uteis.LoadingDialog
import com.engefour.strateponto.viewpager.ViewPagerLeaderboardAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_leaderboard.view.*


class LeaderboardFragment : androidx.fragment.app.Fragment() {

    private var listener: OnFragmentInteractionListener? = null
    lateinit var loadingDialog: LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(null)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loadingDialog = LoadingDialog(activity)
        loadingDialog.showDialog()
        val view =  inflater.inflate(R.layout.fragment_leaderboard, container, false)
        val fragmentAdapter = ViewPagerLeaderboardAdapter(childFragmentManager)
        view.viewPagerLeaderboard.isSaveFromParentEnabled = true
        view.viewPagerLeaderboard.isSaveEnabled = true
        view.viewPagerLeaderboard.adapter = fragmentAdapter

        //Sincroniza tabsLeaderboard com o viewPagerLeaderboard
        view.tabsLeaderboard.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(view.viewPagerLeaderboard))
        view.viewPagerLeaderboard.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(view.tabsLeaderboard))

        return view
    }

    interface AtualizarListaLeaderboard{
        fun atualizarListaLeaderboard()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(): LeaderboardFragment {
            return LeaderboardFragment()
        }
    }
}