package com.engefour.strateponto.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.engefour.strateponto.activities.GerenciarActivity
import com.engefour.strateponto.R
import com.engefour.strateponto.uteis.LoadingDialog
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_admin.view.*
import java.util.HashMap

class AdminFragment : androidx.fragment.app.Fragment() {

    private var listener: OnFragmentInteractionListener? = null
    lateinit var loadingDialog: LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_admin, container, false)
        val db = FirebaseFirestore.getInstance()
        loadingDialog = LoadingDialog(activity)

        view.buttonChave.setOnClickListener{
            //Chama o popup de troca e acessa os elementos
            loadingDialog.showDialog()
            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCanceledOnTouchOutside(true)
            val dialogView = LayoutInflater.from(activity).inflate(R.layout.popup_alterar_chave,null)
            val editTextNovaChave = dialogView.findViewById<EditText>(R.id.editTextNovaChave)
            val mudarChaveButton = dialogView.findViewById<Button>(R.id.mudarChaveButton)
            val textViewChave = dialogView.findViewById<TextView>(R.id.textViewChave)
            db.document("sala/estadoSala").get().addOnSuccessListener {
                textViewChave.text = it.getString("chaveDeAcesso")
                loadingDialog.hideDialog()
            }
            mudarChaveButton.setOnClickListener {
                val novaChave = editTextNovaChave.text.toString()
                if(novaChave.isEmpty()){
                    Toast.makeText(activity, getString(R.string.login_empty), Toast.LENGTH_SHORT).show()
                } else {
                    val builder = AlertDialog.Builder(activity)
                    builder.setTitle("Mudar Chave")
                    builder.setMessage("Tem certeza que deseja mudar a chave?")
                    builder.setPositiveButton("Sim") { _, _ ->
                        loadingDialog.showDialog()
                        FirebaseFirestore.getInstance().document("sala/estadoSala").get().addOnSuccessListener {
                            val chave = HashMap<String, Any>()
                            chave["chaveDeAcesso"] = novaChave
                            FirebaseFirestore.getInstance().document("sala/estadoSala")
                                .update(chave).addOnSuccessListener {
                                    loadingDialog.hideDialog()
                                    dialog.cancel()
                                    Toast.makeText(activity,"Chave Alterada!", Toast.LENGTH_LONG).show()
                                }
                                .addOnFailureListener {
                                    loadingDialog.hideDialog()
                                    Toast.makeText(activity,"Falha ao alterar chave: "+it.message, Toast.LENGTH_LONG).show()
                                }
                        }
                    }
                    builder.setNegativeButton("Cancelar") { _, _ -> }
                    val alertDialog: AlertDialog = builder.create()
                    alertDialog.show()
                }
            }
            dialog.setContentView(dialogView)
            dialog.show()
        }
        view.buttonGerenciar.setOnClickListener{
            val i = Intent(activity, GerenciarActivity::class.java)
            startActivity(i)
        }
        view.buttonNotificacao.setOnClickListener{
            //Chama o popup de troca e acessa os elementos
            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCanceledOnTouchOutside(true)
            val dialogView = LayoutInflater.from(activity).inflate(R.layout.popup_mandar_notificacao,null)
            val editTextTitulo = dialogView.findViewById<EditText>(R.id.editTextTitulo)
            val editTextDescricao = dialogView.findViewById<EditText>(R.id.editTextDescricao)
            val mandarNotificacaoButton = dialogView.findViewById<Button>(R.id.mandarNotificacaoButton)
            mandarNotificacaoButton.setOnClickListener {
                val tituloNotificacao = editTextTitulo.text.toString()
                val descricaoNotificacao = editTextDescricao.text.toString()
                if(tituloNotificacao.isEmpty() || descricaoNotificacao.isEmpty()){
                    Toast.makeText(activity, getString(R.string.login_empty), Toast.LENGTH_SHORT).show()
                } else {
                    val builder = AlertDialog.Builder(activity)
                    builder.setTitle(tituloNotificacao)
                    builder.setMessage(descricaoNotificacao)
                    builder.setPositiveButton("Mandar!") { _, _ ->
                        loadingDialog.showDialog()
                        val notificacao = HashMap<String, Any>()
                        notificacao["tituloNotificacao"] = tituloNotificacao
                        notificacao["descricaoNotificacao"] = descricaoNotificacao
                        FirebaseFirestore.getInstance()
                            .document("sala/notificacao").update(notificacao).addOnSuccessListener {
                                loadingDialog.hideDialog()
                                dialog.cancel()
                                Toast.makeText(activity,"Notificação Enviada!", Toast.LENGTH_LONG).show()
                            }
                            .addOnFailureListener {
                                loadingDialog.hideDialog()
                                Toast.makeText(activity,"Falha ao enviar notificação: "+it.message, Toast.LENGTH_LONG).show()
                            }

                    }
                    builder.setNegativeButton("Cancelar") { _, _ -> }
                    val alertDialog: AlertDialog = builder.create()
                    alertDialog.show()
                }
            }
            dialog.setContentView(dialogView)
            dialog.show()
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(): AdminFragment {
            return AdminFragment()
        }
    }
}