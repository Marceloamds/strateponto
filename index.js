'use-strict'
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.sendNotificationEstadoSala = functions.firestore.document("sala/estadoSala").onUpdate(event=> {

  return admin.firestore().collection("sala").doc("estadoSala").get().then(queryResult => {
    const estado = queryResult.data().estadoSala.toString()
    if(estado === "Reunião com cliente"){
      const message = {
        notification: {
          title: "Tem um cliente na sala!",
          body: 'Por favor, só entre na sala caso necessário',
          sound: "default"
       }
      };
    
      const options = {
       priority: "high",
        timeToLive: 60*60*2
     };
    
    return admin.messaging().sendToTopic("all",message,options);
  }else if(estado === "Reunião de diretoria/gerência"){
    const message = {
      notification: {
        title: "Está tendo uma reunião na sala!",
        body: 'Por favor, não faça barulhos ao entrar',
        sound: "default"
     }
    };
  
    const options = {
     priority: "high",
      timeToLive: 60*60*2
   };

  return admin.messaging().sendToTopic("all",message,options);

} else{
  return true
}
});

});

exports.sendNotificationCafe = functions.firestore.document("sala/cafe").onUpdate(event=> {
      const message = {
        notification: {
          title: "Tem café fresquinho na sala!",
          body:'Acabaram de fazer um café na sala...\nChega aí pra tomar!',
          sound: "default"
        }
      };

    
      const options = {
       priority: "high",
        timeToLive: 60*60*2
     };
    
    return admin.messaging().sendToTopic("all",message,options);
});